using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ININ.Integrations.Windows.Base;
using ININ.ThinClient.Common;
using ININ.ThinClient.Common.Win32;

namespace DialFromClipboard
{
    internal class ClientCommunicationManager : ININ.Integrations.ICallControlImpl, IDisposable
    {
        public static ClientCommunicationManager Instance
        {
            get
            {
                lock (s_syncObject)
                {
                    if (s_instance == null)
                    {
                        s_instance = new ClientCommunicationManager();
                    }
                }
                return s_instance;
            }
        }
        private static Object s_syncObject = "Locker";
        private static ClientCommunicationManager s_instance = null;
        private List<DialForm> m_clientsToMonitor = new List<DialForm>();
        private ININ.Integrations.CallControlBase m_callControl = null;

        public delegate void ConnectionStateChangedDelegate(bool NewState);
        public event ConnectionStateChangedDelegate ConnectionStateChanged;

        public ININ.Integrations.CallControlBase ClientCallControlBase
        {
            get
            {
                return m_callControl;
            }
        }

        #region Forms that we're monitoring.
        public bool AddClient(DialForm dialForm)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("ClientCommunicationManager.AddClient"))
            {
                bool bRet = false;
                try
                {
                    if (dialForm != null)
                    {
                        lock (m_clientsToMonitor)
                        {
                            if (m_clientsToMonitor.Contains(dialForm) == false)
                            {
                                m_clientsToMonitor.Add(dialForm);
                            }
                            if (m_callControl == null)
                            {
                                TraceTopics.DialFromClipboardTrace.status("Creating CallControlBase instance.");
                                m_callControl = new ININ.Integrations.CallControlBase(this);
                            }

                            bRet = true;
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex, "Exception caught in ClientCommunicationManager.AddClient");
                }
                TraceTopics.DialFromClipboardTrace.verbose("Return: {}", bRet);
                return bRet;
            }
        }
        public bool RemoveClient(DialForm dialForm)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("ClientCommunicationManager.RemoveClient"))
            {
                bool bRet = false;
                try
                {
                    if (dialForm != null)
                    {
                        lock (m_clientsToMonitor)
                        {
                            if (m_clientsToMonitor.Contains(dialForm) == true)
                            {
                                m_clientsToMonitor.Remove(dialForm);
                            }
                            if (m_clientsToMonitor.Count == 0)
                            {
                                if (m_callControl != null)
                                {
                                    IDisposable DisposableCallControl = (IDisposable)m_callControl;
                                    if (DisposableCallControl != null)
                                    {
                                        TraceTopics.DialFromClipboardTrace.status("Disposing CallControlBase instance.");
                                        DisposableCallControl.Dispose();
                                        m_callControl = null;
                                    }
                                }
                            }
                            bRet = true;
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex, "Exception caught in ClientCommunicationManager.RemoveClient");
                }
                TraceTopics.DialFromClipboardTrace.verbose("Return: {}", bRet);
                return bRet;
            }
        }
        public bool StopMonitoringAllClients()
        {
            using (TraceTopics.DialFromClipboardTrace.scope("ClientCommunicationManager.StopMonitoringAllClients"))
            {
                bool bRet = false;
                try
                {
                    lock (m_clientsToMonitor)
                    {
                        m_clientsToMonitor.Clear();
                    }
                    bRet = true;
                }
                catch (System.Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex, "Exception caught in ClientCommunicationManager.StopMonitoringAllClients");
                }
                TraceTopics.DialFromClipboardTrace.verbose("Return: {}", bRet);
                return bRet;
            }

        }
        /// <summary>
        /// Returns the currently active DialForm
        /// </summary>
        public DialForm ActiveClient
        {
            get
            {
                lock (m_clientsToMonitor)
                {
                    foreach (DialForm dialForm in m_clientsToMonitor)
                    {
                        if ((dialForm != null))
                        {
                            return dialForm;
                        }
                    }
                }
                return null;
            }
        }
        #endregion

        #region ICallControlImpl Members
        public string GetAppName()
        {
            using (TraceTopics.DialFromClipboardTrace.scope("GetAppName"))
            {
                string ProcName = ProcessInfo.GetProcessName();
                TraceTopics.DialFromClipboardTrace.verbose("Return: {}", ProcName);
                return ProcName;
            }
        }

        public string[] GetWatchedAttributes()
        {
            return new string[0];
        }

        public void PerformCustomAction(string p_Command, ININ.ThinClient.ClientLib.Session p_Session)
        {
        }

        public bool PopInteraction(ININ.ThinClient.ClientLib.Session p_Session, ININ.ThinClient.Interaction p_Interaction)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("ClientBrowserHelperObjectManager.PopInteraction"))
            {
                TraceTopics.DialFromClipboardTrace.verbose("Return: {}", false);
                return false;
            }
        }

        public void UpdateButtonState(ININ.Integrations.ButtonStateParams p_Params)
        {
        }

        public void UpdateButtonVisibility(int p_Capabilities)
        {
        }

        public void UpdateCallerID(string p_RemoteID, string p_RemoteName)
        {
        }

        public void UpdateLoginInfo(bool p_Connected, ININ.ThinClient.ClientLib.Session p_Session)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("ClientBrowserHelperObjectManager.UpdateLoginInfo"))
            {
                if (m_bConnectedToClient != p_Connected)
                {
                    m_bConnectedToClient = p_Connected;
                    if (ConnectionStateChanged != null)
                    {
                        try
                        {
                            ConnectionStateChanged(m_bConnectedToClient);
                        }
                        catch (System.Exception ex)
                        {
                            TraceTopics.DialFromClipboardTrace.exception(ex, "Exception caught firing connection state changed event.");
                        }
                    }
                }
            }
        }

        public bool ConnectedToClient
        {
            get
            {
                return m_bConnectedToClient;
            }
        }
        private bool m_bConnectedToClient = false;

        #endregion

        #region IDisposable Members
        void IDisposable.Dispose()
        {
            StopMonitoringAllClients();
            m_callControl = null;
        }
        #endregion
    }
}
