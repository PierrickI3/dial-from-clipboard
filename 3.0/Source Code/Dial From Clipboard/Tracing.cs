using System;
using System.Collections.Generic;
using System.Text;

namespace DialFromClipboard
{
    public class Tracing : TopicTracer
    {
        /// <exclude />
        public static int hdl = I3Trace.initialize_topic("Dial From Clipboard");
        /// <exclude />
        public override int get_handle() { return hdl; }
    }
    /// <exclude />
    public static class TraceTopics
    {
        static TraceTopics()
        {
            Init();
        }

        private static bool _Init = false;
        /// <exclude />
        public static void Init()
        {
            if (!_Init)
            {
                I3Trace.initialize_default_sinks();
                _Init = true;
            }
        }

        private static Tracing _DialFromClipboardTrace = new Tracing();
        public static Tracing DialFromClipboardTrace { get { return _DialFromClipboardTrace; } }
    }
}
