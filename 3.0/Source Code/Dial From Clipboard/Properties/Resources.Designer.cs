﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3623
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DialFromClipboard.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("DialFromClipboard.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can&apos;t Dial.
        /// </summary>
        internal static string CANT_DIAL {
            get {
                return ResourceManager.GetString("CANT_DIAL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Connected.
        /// </summary>
        internal static string CONNECTED {
            get {
                return ResourceManager.GetString("CONNECTED", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Use CTRL+ATL+X from any application to dial the copied text.
        /// </summary>
        internal static string CONNECTED_TO_CLIENT {
            get {
                return ResourceManager.GetString("CONNECTED_TO_CLIENT", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Searching existing .Net client session.
        /// </summary>
        internal static string CONNECTING_TO_CLIENT {
            get {
                return ResourceManager.GetString("CONNECTING_TO_CLIENT", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dialing.
        /// </summary>
        internal static string DIALING {
            get {
                return ResourceManager.GetString("DIALING", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error.
        /// </summary>
        internal static string ERROR {
            get {
                return ResourceManager.GetString("ERROR", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error While Dialing.
        /// </summary>
        internal static string ERROR_WHILE_DIALING {
            get {
                return ResourceManager.GetString("ERROR_WHILE_DIALING", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Exception occured.
        /// </summary>
        internal static string EXCEPTION_OCCURED {
            get {
                return ResourceManager.GetString("EXCEPTION_OCCURED", resourceCulture);
            }
        }
        
        internal static System.Drawing.Icon Icon_Error {
            get {
                object obj = ResourceManager.GetObject("Icon_Error", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        internal static System.Drawing.Icon Icon_Info {
            get {
                object obj = ResourceManager.GetObject("Icon_Info", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        internal static System.Drawing.Icon Icon_NotConnected {
            get {
                object obj = ResourceManager.GetObject("Icon_NotConnected", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        internal static System.Drawing.Icon Icon_OK {
            get {
                object obj = ResourceManager.GetObject("Icon_OK", resourceCulture);
                return ((System.Drawing.Icon)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Initializing....
        /// </summary>
        internal static string INITIALIZING {
            get {
                return ResourceManager.GetString("INITIALIZING", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dial &apos;.
        /// </summary>
        internal static string MENU_DIAL {
            get {
                return ResourceManager.GetString("MENU_DIAL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Exit.
        /// </summary>
        internal static string MENU_EXIT {
            get {
                return ResourceManager.GetString("MENU_EXIT", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No Data Found in Clipboard.
        /// </summary>
        internal static string MENU_NO_NUMBER_FOUND {
            get {
                return ResourceManager.GetString("MENU_NO_NUMBER_FOUND", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Not connected to a client.
        /// </summary>
        internal static string NOT_CONNECTED_TO_CLIENT {
            get {
                return ResourceManager.GetString("NOT_CONNECTED_TO_CLIENT", resourceCulture);
            }
        }
        
        internal static System.Drawing.Bitmap TaskBarClose {
            get {
                object obj = ResourceManager.GetObject("TaskBarClose", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        internal static System.Drawing.Bitmap TaskBarSkin {
            get {
                object obj = ResourceManager.GetObject("TaskBarSkin", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Warning.
        /// </summary>
        internal static string WARNING {
            get {
                return ResourceManager.GetString("WARNING", resourceCulture);
            }
        }
    }
}
