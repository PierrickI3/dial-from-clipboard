using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using ININ.Integrations.Windows.Base;
using ININ.Integrations;

namespace DialFromClipboard
{
    public partial class DialForm : Form
    {
        #region Private Properties
        private MenuItem miDial, miExit;
        private System.Windows.Forms.IDataObject iData = new DataObject();
        private Kennedy.ManagedHooks.KeyboardHook keyboardHook = null;
        private bool bControlPressed = false, bAltPressed = false, bLetterPressed = false;
        private const int WM_DRAWCLIPBOARD = 776;
        #endregion

        #region Initialization
        public DialForm()
        {
            InitializeComponent();
            InitializeForm();
            InitializeClientConnection();
        }
        private void InitializeForm()
        {
            using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.InitializeForm"))
            {
                try
                {
                    // CONTEXT MENU
                    TraceTopics.DialFromClipboardTrace.note("Initializing Context Menus");
                    ContextMenu cm = new ContextMenu();
                    miDial = new MenuItem(Properties.Resources.NOT_CONNECTED_TO_CLIENT, new EventHandler(DialMenuItem_Click));
                    miDial.Enabled = false;
                    cm.MenuItems.Add(miDial);
                    miExit = new MenuItem(Properties.Resources.MENU_EXIT, new EventHandler(ExitMenuItem_Click));
                    miExit.Enabled = true;
                    cm.MenuItems.Add(miExit);
                    notifyIcon1.ContextMenu = cm;

                    // INIT KEYBOARD HOOKS
                    TraceTopics.DialFromClipboardTrace.note("Initializing Keyboard Hooks");
                    keyboardHook = new Kennedy.ManagedHooks.KeyboardHook();
                    keyboardHook.KeyboardEvent += new Kennedy.ManagedHooks.KeyboardHook.KeyboardEventHandler(keyboardHook_KeyboardEvent);
                    keyboardHook.InstallHook();

                    // INIT CLIPBOARD MONITOR
                    TraceTopics.DialFromClipboardTrace.note("Initializing Clipboard Monitoring");
                    SetClipboardViewer(this.Handle.ToInt32());

                    TraceTopics.DialFromClipboardTrace.note("Form Initialized");
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex);
                    if (MessageBox.Show(Properties.Resources.EXCEPTION_OCCURED + ":" + ex.Message + " from " + ex.Source, Properties.Resources.ERROR, MessageBoxButtons.OK, MessageBoxIcon.Error) == DialogResult.OK)
                        ExitApplication();
                }
            }
        }
        private void ConnectionUp()
        {
            SetStatus(Properties.Resources.CONNECTED, Properties.Resources.CONNECTED_TO_CLIENT, ToolTipIcon.Info, Properties.Resources.Icon_OK, 1000);
            UpdateStatus();
        }
        private void ConnectionDown()
        {
            SetStatus(Properties.Resources.WARNING, Properties.Resources.NOT_CONNECTED_TO_CLIENT, ToolTipIcon.Warning, Properties.Resources.Icon_NotConnected, 3000);
            UpdateStatus();
        }
        private void InitializeClientConnection()
        {
            using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.InitializeClientConnection"))
            {
                try
                {
                    TraceTopics.DialFromClipboardTrace.note("Initializing Client Connection");
                    SetStatus(Properties.Resources.INITIALIZING, Properties.Resources.CONNECTING_TO_CLIENT, ToolTipIcon.Info, Properties.Resources.Icon_NotConnected, 1000);
                    ClientCommunicationManager.Instance.ConnectionStateChanged += new ClientCommunicationManager.ConnectionStateChangedDelegate(Instance_ConnectionStateChanged);
                    ClientCommunicationManager.Instance.AddClient(this);
                    TraceTopics.DialFromClipboardTrace.note("Client Connection Initialized. Check connection state events in next messages.");
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.error(ex.Message);
                    SetStatus(Properties.Resources.EXCEPTION_OCCURED, ex.Message + " from " + ex.Source, ToolTipIcon.Error, Properties.Resources.Icon_Error, 2000);
                }
                finally
                {
                    TraceTopics.DialFromClipboardTrace.note("Initialization Successful");
                }
            }
        }
        #endregion

        #region Clipboard
        [DllImport("user32.dll")]
        public static extern int SetClipboardViewer(int windowHandle);
        private string GetClipboardData()
        {
            string strReturn = String.Empty;
            using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.GetClipboardData"))
            {
                try
                {
                    TraceTopics.DialFromClipboardTrace.note("Getting data from clipboard");
                    iData = Clipboard.GetDataObject();
                    TraceTopics.DialFromClipboardTrace.note("Successfully called GetDataObject()");
                    if (iData != null && iData.GetDataPresent(DataFormats.StringFormat))
                    {
                        TraceTopics.DialFromClipboardTrace.note("GetDataObject() returned a string object");
                        strReturn = (string)iData.GetData(DataFormats.StringFormat);
                        TraceTopics.DialFromClipboardTrace.note("Found data in clipboard: " + strReturn);
                        if (strReturn.Length > 50)
                        {
                            strReturn = strReturn.Substring(0, 50);
                            TraceTopics.DialFromClipboardTrace.note("After limiting data to 50 characters: " + strReturn);
                        }
                    }
                    else
                    {
                        TraceTopics.DialFromClipboardTrace.note("Clipboard data is not a string");
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex);
                    SetStatus(Properties.Resources.EXCEPTION_OCCURED, ex.Message, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                }
                finally
                {
                    TraceTopics.DialFromClipboardTrace.note("Returning: " + strReturn);
                }
            }
            return strReturn;
        }
        protected override void WndProc(ref Message m)
        {
            try
            {
                base.WndProc(ref m);
                if (m.Msg == WM_DRAWCLIPBOARD)
                    UpdateStatus();
            }
            catch (Exception ex)
            {
                using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.WndProc"))
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex);
                    SetStatus(Properties.Resources.EXCEPTION_OCCURED, ex.Message, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                }
            }
        }
        private bool AnyValidDataInClipboard()
        {
            string strData = GetClipboardData();
            if (strData != null && strData.Length > 0)
                return true;
            else
                return false;
        }
        #endregion

        #region Keyboard Hooks
        void keyboardHook_KeyboardEvent(Kennedy.ManagedHooks.KeyboardEvents kEvent, Keys key)
        {
            if (key == Keys.Control)
            {
                if (kEvent == Kennedy.ManagedHooks.KeyboardEvents.KeyDown || kEvent == Kennedy.ManagedHooks.KeyboardEvents.SystemKeyDown)
                    bControlPressed = true;
                else
                    bControlPressed = false;
            }
            else if (key == Keys.Alt)
            {
                if (kEvent == Kennedy.ManagedHooks.KeyboardEvents.KeyDown || kEvent == Kennedy.ManagedHooks.KeyboardEvents.SystemKeyDown)
                    bAltPressed = true;
                else
                    bAltPressed = false;
            }
            else if (key == Keys.X)
            {
                if (kEvent == Kennedy.ManagedHooks.KeyboardEvents.KeyDown)
                    bLetterPressed = true;
                else
                    bLetterPressed = false;
            }
            if (bControlPressed && bAltPressed && bLetterPressed)
            {
                using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.keyboardHook_KeyboardEvent"))
                {
                    try
                    {
                        TraceTopics.DialFromClipboardTrace.note("CTRL+ALT+X has been detected. Uninstalling hook to prevent repeats.");
                        keyboardHook.UninstallHook();
                        TraceTopics.DialFromClipboardTrace.note("Calling DialNumber()");
                        DialNumber(bLetterPressed); // If letter Pressed, dial from clipboard
                        TraceTopics.DialFromClipboardTrace.note("Sleeping: Start.");
                        Thread.Sleep(1000);
                        TraceTopics.DialFromClipboardTrace.note("Sleeping: Done. Resetting control variables.");
                        bControlPressed = false;
                        bAltPressed = false;
                        bLetterPressed = false;
                        TraceTopics.DialFromClipboardTrace.note("Re-installing keyboard hook.");
                        keyboardHook.InstallHook();
                    }
                    catch (Exception ex)
                    {
                        TraceTopics.DialFromClipboardTrace.exception(ex);
                        SetStatus(Properties.Resources.EXCEPTION_OCCURED, ex.Message, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                    }
                }
            }
        }
        #endregion

        #region ININ Methods (Connection and Dialing)
        private bool IsConnectedToClient()
        {
            if (ClientCommunicationManager.Instance != null && ClientCommunicationManager.Instance.ConnectedToClient)
                return true;
            else
                return false;
        }
        void Instance_ConnectionStateChanged(bool NewState)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.Instance_ConnectionStateChanged"))
            {
                TraceTopics.DialFromClipboardTrace.note("New Connection State detected: " + NewState.ToString());
                if (NewState != false)
                    ConnectionUp();
                else
                    ConnectionDown();
            }
        }
        private void DialNumber(bool bUseClipboard)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.DialNumber"))
            {
                try
                {
                    string sNumber = String.Empty;
                    if (bUseClipboard)
                    {
                        TraceTopics.DialFromClipboardTrace.note("Getting data from clipboard");
                        sNumber = GetClipboardData();
                    }
                    else
                    {
                        TraceTopics.DialFromClipboardTrace.note("Getting data from focused application");
                        SelectedText st = new SelectedText();
                        sNumber = st.GetSelectedText();
                    }

                    if (sNumber != null && sNumber.Length > 0)
                        DialNumber(sNumber);
                    else
                        TraceTopics.DialFromClipboardTrace.warning("No text to dial was found.");
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex);
                    SetStatus(Properties.Resources.EXCEPTION_OCCURED, ex.Message, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                }
            }
        }
        private void DialNumber(string sNumber)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.DialNumber(" + sNumber + ")"))
            {
                try
                {
                    if (IsConnectedToClient())
                    {
                        ININ.Integrations.CallControlBase ccb = ClientCommunicationManager.Instance.ClientCallControlBase;
                        if (ccb != null)
                        {
                            MakeCallAction mca = new MakeCallAction(ccb);
                            mca.NumberToDial = sNumber;
                            TraceTopics.DialFromClipboardTrace.note("Calling: " + sNumber);
                            SetStatus(Properties.Resources.DIALING, sNumber, ToolTipIcon.Info, Properties.Resources.Icon_OK, 1000);
                            mca.PerformAction();
                        }
                        else
                        {
                            TraceTopics.DialFromClipboardTrace.error("CallControlBased is null. Cannot dial");
                            SetStatus(Properties.Resources.ERROR, String.Format("{0}: {1}", Properties.Resources.CANT_DIAL, Properties.Resources.NOT_CONNECTED_TO_CLIENT), ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                        }

                    }
                    else
                    {
                        SetStatus(Properties.Resources.CANT_DIAL, Properties.Resources.NOT_CONNECTED_TO_CLIENT, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                    }
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex, "Exception in {0}", "DialFromClipboard.DialNumber");
                    SetStatus(Properties.Resources.EXCEPTION_OCCURED, ex.Message, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                }
            }
        }
        #endregion

        #region Form Methods
        private void SetStatus(string strTitle, string strMessage, ToolTipIcon icoTooltip, Icon icoNotify, int iTimeout)
        {
            try
            {
                notifyIcon1.BalloonTipTitle = strTitle;
                notifyIcon1.BalloonTipText = strMessage;

                lblStatus.Text = strTitle + ":" + strMessage;

                if (icoNotify != null)
                    notifyIcon1.Icon = icoNotify;

                if (icoTooltip != ToolTipIcon.None)
                {
                    notifyIcon1.BalloonTipIcon = icoTooltip;
                    notifyIcon1.ShowBalloonTip(iTimeout);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("SetStatus: " + ex.Message);
            }

        }
        private void UpdateStatus()
        {
            if (!IsConnectedToClient())
            {
                // UPDATE MENUS
                miDial.Text = Properties.Resources.NOT_CONNECTED_TO_CLIENT;
                miDial.Enabled = false;

                // UPDATE FORM
                txtNumber.Text = "";
                btnDial.Enabled = false;
            }
            else
            {
                if (AnyValidDataInClipboard())
                {
                    string strData = GetClipboardData();

                    // UPDATE MENUS
                    miDial.Text = String.Format("{0}{1}'", Properties.Resources.MENU_DIAL, strData);
                    miDial.Enabled = true;

                    // UPDATE FORM
                    txtNumber.Text = strData;
                    btnDial.Enabled = true;
                }
                else
                {
                    // UPDATE MENUS
                    miDial.Text = Properties.Resources.MENU_NO_NUMBER_FOUND;
                    miDial.Enabled = false;

                    // UPDATE FORM
                    txtNumber.Text = "";
                    btnDial.Enabled = false;
                }
            }
        }

        private void btnDial_Click(object sender, EventArgs e)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.btnDial_Click"))
            {
                TraceTopics.DialFromClipboardTrace.note("Dial button has been clicked. txtNumber contains: " + txtNumber.Text);
                if (txtNumber.Text.Length > 0)
                    DialNumber(txtNumber.Text);
            }
        }
        private void DialForm_Load(object sender, EventArgs e)
        {
            Hide();
        }
        private void DialForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                this.Hide();
                notifyIcon1.Visible = true;
            }
        }
        private void DialMenuItem_Click(Object sender, System.EventArgs e)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.DialMenuItem_Click"))
            {
                TraceTopics.DialFromClipboardTrace.note("Dial menu item has been clicked. Calling DialNumber()");
                DialNumber(true);
            }
        }
        private void ExitMenuItem_Click(Object sender, System.EventArgs e)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.ExitMenuItem_Click"))
            {
                TraceTopics.DialFromClipboardTrace.note("Exit Menu item has been clicked. Calling ExitApplication()");
                ExitApplication();
            }
        }
        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = false;

            this.Activate();
            this.Focus();
        }
        private void ExitApplication()
        {
            using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.ExitMenuItem_Click"))
            {
                try
                {
                    TraceTopics.DialFromClipboardTrace.note("Stopping monitoring on all clients");
                    ClientCommunicationManager.Instance.StopMonitoringAllClients();
                    TraceTopics.DialFromClipboardTrace.note("Removing client instance");
                    ClientCommunicationManager.Instance.RemoveClient(this);
                    TraceTopics.DialFromClipboardTrace.note("Disposing");
                    notifyIcon1.Dispose();
                    this.Dispose(true);
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex);
                    MessageBox.Show("ExitApplication: " + ex.Message);
                }
            }
        }
        #endregion
    }
}