using System;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;

namespace DialFromClipboard
{
    [RunInstaller(true)]
    public partial class Installer1 : Installer
    {
        public Installer1()
        {
            InitializeComponent();
        }
        public override void Install(System.Collections.IDictionary stateSaver)
        {
            base.Install(stateSaver);
        }
        public override void Commit(System.Collections.IDictionary savedState)
        {
            base.Commit(savedState);
            //System.Diagnostics.Process.Start(Context.Parameters["CLIENTDIR"] + "Dial From Clipboard.exe");
        }
        public override void Rollback(System.Collections.IDictionary savedState)
        {
            base.Rollback(savedState);
        }
        public override void Uninstall(System.Collections.IDictionary savedState)
        {
            //Kill application if running
            Process[] ps = Process.GetProcessesByName("Dial From Clipboard");

            foreach (Process pr in ps)
            {
                pr.Kill();
                pr.Close();
            }

            base.Uninstall(savedState);
        }
    }
}