using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Forms;
using ININ.IceLib.Connection;
using ININ.ThinClient.Common;
using ININ.IceLib.People;

namespace ININ.Integrations.RemoteCommands
{
    internal class ClientCommunicationManager : ININ.Integrations.ICallControlImpl, IDisposable
    {
        #region Private
        private static Object s_syncObject = "Locker";
        private static ClientCommunicationManager s_instance = null;
        private List<MainForm> m_clientsToMonitor = new List<MainForm>();
        private ININ.Integrations.CallControlBase m_callControl = null;
        private ININ.ThinClient.ClientLib.Session _currentSession = null;
        private ININ.IceLib.Connection.Session _icelibSession = null;
        private HostSettings hostSettings = null;
        private AuthSettings authSettings = null;
        private StationSettings stationSettings = null;
        private SessionSettings sessionSettings = null;
        private PeopleManager peopleManager = null;
        private bool m_bConnectedToClient = false;
        private bool _connectedToIceSession = false;
        #endregion

        #region Public
        public static ClientCommunicationManager Instance
        {
            get
            {
                lock (s_syncObject)
                {
                    if (s_instance == null)
                    {
                        s_instance = new ClientCommunicationManager();
                    }
                }
                return s_instance;
            }
        }
        public delegate void ConnectionStateChangedDelegate(bool NewState);
        public event ConnectionStateChangedDelegate ConnectionStateChanged;
        public ININ.Integrations.CallControlBase ClientCallControlBase
        {
            get
            {
                return m_callControl;
            }
        }
        public ININ.ThinClient.ClientLib.Session CurrentSession
        {
            get
            {
                return _currentSession;
            }
        }
        public ININ.IceLib.Connection.Session IceLibSession
        {
            get
            {
                return _icelibSession;
            }
        }
        public bool ConnectedToClient
        {
            get
            {
                return m_bConnectedToClient;
            }
        }
        public bool ConnectedToIceSession
        {
            get
            {
                return _connectedToIceSession;
            }
        }
        #endregion

        #region Monitored Forms
        public bool AddClient(MainForm dialForm)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("ClientCommunicationManager.AddClient"))
            {
                bool bRet = false;
                try
                {
                    if (dialForm != null)
                    {
                        lock (m_clientsToMonitor)
                        {
                            if (m_clientsToMonitor.Contains(dialForm) == false)
                            {
                                m_clientsToMonitor.Add(dialForm);
                            }
                            if (m_callControl == null)
                            {
                                TraceTopics.DialFromClipboardTrace.status("Creating CallControlBase instance.");
                                m_callControl = new ININ.Integrations.CallControlBase(this);
                            }
                            bRet = true;
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex, "Exception caught in ClientCommunicationManager.AddClient");
                }
                TraceTopics.DialFromClipboardTrace.verbose("Return: {}", bRet);
                return bRet;
            }
        }
        public bool RemoveClient(MainForm dialForm)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("ClientCommunicationManager.RemoveClient"))
            {
                bool bRet = false;
                try
                {
                    if (dialForm != null)
                    {
                        lock (m_clientsToMonitor)
                        {
                            if (m_clientsToMonitor.Contains(dialForm) == true)
                            {
                                m_clientsToMonitor.Remove(dialForm);
                            }
                            if (m_clientsToMonitor.Count == 0)
                            {
                                if (m_callControl != null)
                                {
                                    IDisposable DisposableCallControl = (IDisposable)m_callControl;
                                    if (DisposableCallControl != null)
                                    {
                                        TraceTopics.DialFromClipboardTrace.status("Disposing CallControlBase instance.");
                                        DisposableCallControl.Dispose();
                                        m_callControl = null;
                                    }
                                }
                            }
                            bRet = true;
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex, "Exception caught in ClientCommunicationManager.RemoveClient");
                }
                TraceTopics.DialFromClipboardTrace.verbose("Return: {}", bRet);
                return bRet;
            }
        }
        public bool StopMonitoringAllClients()
        {
            using (TraceTopics.DialFromClipboardTrace.scope("ClientCommunicationManager.StopMonitoringAllClients"))
            {
                bool bRet = false;
                try
                {
                    lock (m_clientsToMonitor)
                    {
                        m_clientsToMonitor.Clear();
                    }
                    bRet = true;
                }
                catch (System.Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex, "Exception caught in ClientCommunicationManager.StopMonitoringAllClients");
                }
                TraceTopics.DialFromClipboardTrace.verbose("Return: {}", bRet);
                return bRet;
            }

        }
        public MainForm ActiveClient
        {
            get
            {
                lock (m_clientsToMonitor)
                {
                    foreach (MainForm dialForm in m_clientsToMonitor)
                    {
                        if ((dialForm != null))
                        {
                            return dialForm;
                        }
                    }
                }
                return null;
            }
        }
        #endregion

        #region Ice Session Connect
        public bool Connect()
        {
            using (TraceTopics.DialFromClipboardTrace.scope("ClientCommunicationManager.Connect"))
            {
                try
                {
                    if (_icelibSession == null || _icelibSession.ConnectionState != ConnectionState.Up)
                    {
                        TraceTopics.DialFromClipboardTrace.note("Getting Stored Credentials");
                        StoredCredential storedCredential = ININ.IceLib.Connection.CommonCredentials.GetCredentials();

                        TraceTopics.DialFromClipboardTrace.note("Setting Host Settings");
                        hostSettings = new HostSettings(new HostEndpoint(storedCredential.HostName, storedCredential.HostPort));
                        TraceTopics.DialFromClipboardTrace.note("Setting Station Settings");
                        stationSettings = new WorkstationSettings(storedCredential.Station, SupportedMedia.Call);
                        TraceTopics.DialFromClipboardTrace.note("Setting Session Settings");
                        sessionSettings = new SessionSettings();
                        sessionSettings.ApplicationName = "DialFromClipboard";
                        TraceTopics.DialFromClipboardTrace.note("Setting Authentication Settings");
                        authSettings = new ICAuthSettings(storedCredential.UserId, storedCredential.Password);

                        // Connect
                        if (_icelibSession == null)
                            _icelibSession = new Session();
                        _icelibSession.ConnectionStateChanged += new ConnectionStateChangedEventHandler(_icelibSession_ConnectionStateChanged);
                        TraceTopics.DialFromClipboardTrace.note("Connecting");
                        _icelibSession.Connect(sessionSettings, hostSettings, authSettings, stationSettings);
                    }
                    if (_icelibSession != null && _icelibSession.ConnectionState == ConnectionState.Up)
                    {
                        _connectedToIceSession = true;
                        peopleManager = PeopleManager.GetInstance(_icelibSession);
                    }
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex);
                }
                return _connectedToIceSession;
            }
        }
        void _icelibSession_ConnectionStateChanged(object sender, ConnectionStateChangedEventArgs e)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("ClientCommunicationManager._icelibSession_ConnectionStateChanged"))
            {
                try
                {
                    TraceTopics.DialFromClipboardTrace.note("Connection State Changed: Connection Status = " + e.State.ToString());
                    if (e.State == ConnectionState.Up)
                    {
                        TraceTopics.DialFromClipboardTrace.note("Connection is up. Assigning IceLibSession and PeopleManager");
                        _icelibSession = (Session)sender;
                        peopleManager = PeopleManager.GetInstance(_icelibSession);
                        _connectedToIceSession = true;

                    }
                    else
                    {
                        _connectedToIceSession = false;
                        TraceTopics.DialFromClipboardTrace.note("Connection is not up. Clearing IceLibSession and PeopleManager");
                        _icelibSession = null;
                        peopleManager = null;
                    }
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex);
                }
            }
        }
        #endregion

        #region Call Control
        public void HoldCall(string strCallId)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("ClientCommunicationManager.HoldCall(" + strCallId + ")"))
            {
                TraceTopics.DialFromClipboardTrace.note("Holding call id: " + strCallId);
                string[] aParameters = { strCallId };
                m_callControl.PerformActionWithParameters(CallControlBase.CommandHold, aParameters);
            }
        }
        public void DisconnectCall(string strCallId)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("ClientCommunicationManager.DisconnectCall(" + strCallId + ")"))
            {
                TraceTopics.DialFromClipboardTrace.note("Disconnecting call id: " + strCallId);
                string[] aParameters = { strCallId };
                m_callControl.PerformActionWithParameters(CallControlBase.CommandDisconnect, aParameters);
            }
        }
        public void ChangeStatus(string strStatusName)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("ClientCommunicationManager.ChangeStatus(" + strStatusName + ")"))
            {
                try
                {
                    if (Connect())
                    {
                        TraceTopics.DialFromClipboardTrace.note("Connecting to People Manager");
                        UserStatusUpdate statusUpdate = new UserStatusUpdate(peopleManager);
                        statusUpdate.StatusId = strStatusName;
                        TraceTopics.DialFromClipboardTrace.note("Changing status to: " + strStatusName);
                        statusUpdate.BeginUpdateRequest(new AsyncCallback(StatusUpdateCompleted), null);
                        TraceTopics.DialFromClipboardTrace.note("Status change processed.");
                    }
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex);
                }
            }
        }
        private void StatusUpdateCompleted(IAsyncResult ar)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("ClientCommunicationManager.StatusUpdateCompleted()"))
            {
                TraceTopics.DialFromClipboardTrace.note("Is Completed?: " + ar.IsCompleted.ToString());
            }
        }
        //public void GetStatuses()
        //{
        //    // CAN'T BE DONE BEFORE 3.0
        //}
        #endregion

        #region ICallControlImpl Members
        public string GetAppName()
        {
            using (TraceTopics.DialFromClipboardTrace.scope("GetAppName"))
            {
                string ProcName = ProcessInfo.GetProcessName();
                TraceTopics.DialFromClipboardTrace.verbose("Return: {}", ProcName);
                return ProcName;
            }
        }
        public string[] GetWatchedAttributes()
        {
            return new string[0];
        }
        public void PerformCustomAction(string p_Command, ININ.ThinClient.ClientLib.Session p_Session)
        {
        }
        public bool PopInteraction(ININ.ThinClient.ClientLib.Session p_Session, ININ.ThinClient.Interaction p_Interaction)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("ClientBrowserHelperObjectManager.PopInteraction"))
            {
                TraceTopics.DialFromClipboardTrace.verbose("Return: {}", false);
                return false;
            }
        }
        public void UpdateButtonState(ININ.Integrations.ButtonStateParams p_Params)
        {
        }
        public void UpdateButtonVisibility(int p_Capabilities)
        {
        }
        public void UpdateCallerID(string p_RemoteID, string p_RemoteName)
        {
        }
        public void UpdateLoginInfo(bool p_Connected, ININ.ThinClient.ClientLib.Session p_Session)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("ClientBrowserHelperObjectManager.UpdateLoginInfo"))
            {
                _currentSession = p_Session;
                if (m_bConnectedToClient != p_Connected)
                {
                    m_bConnectedToClient = p_Connected;
                    if (ConnectionStateChanged != null)
                    {
                        try
                        {
                            ConnectionStateChanged(m_bConnectedToClient);
                        }
                        catch (System.Exception ex)
                        {
                            TraceTopics.DialFromClipboardTrace.exception(ex, "Exception caught firing connection state changed event.");
                        }
                    }
                }
            }
        }
        #endregion

        #region IDisposable Members
        void IDisposable.Dispose()
        {
            this.Dispose();
        }
        public void Dispose()
        {
            StopMonitoringAllClients();
            if (_icelibSession != null)
            {
                if (_icelibSession.ConnectionState == ConnectionState.Up || _icelibSession.ConnectionState == ConnectionState.Attempting)
                    _icelibSession.Disconnect();
            }
            m_callControl = null;
            _currentSession = null;
            _icelibSession = null;
        }
        #endregion

    }
}
