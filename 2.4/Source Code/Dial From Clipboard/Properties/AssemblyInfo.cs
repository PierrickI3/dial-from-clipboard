﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ININ.Integrations.RemoteCommands")]
[assembly: AssemblyDescription("Use this application to dial a number from any application, using DDE or from the clipboard.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Interactive Intelligence")]
[assembly: AssemblyProduct("ININ.Integrations.RemoteCommands")]
[assembly: AssemblyCopyright("Copyright © Interactive Intelligence 2009")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("420d8abe-7abc-4987-902e-24e65ad766a4")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("2.0.0.0")]
[assembly: AssemblyFileVersion("2.0.0.0")]
[assembly: NeutralResourcesLanguageAttribute("en-US")]
