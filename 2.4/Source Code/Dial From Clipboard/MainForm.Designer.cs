namespace ININ.Integrations.RemoteCommands
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                this.notifyIcon1.Dispose();
                this.keyboardHook.Dispose();
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.lblPhoneNumber = new System.Windows.Forms.Label();
            this.txtPhoneNumber = new System.Windows.Forms.TextBox();
            this.btnDial = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.btnHold = new System.Windows.Forms.Button();
            this.cmbStatuses = new System.Windows.Forms.ComboBox();
            this.lblStatusName = new System.Windows.Forms.Label();
            this.btnChangeStatus = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnHold_CallId = new System.Windows.Forms.Button();
            this.txtHold_CallId = new System.Windows.Forms.TextBox();
            this.lblHold_CallId = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDisconnect_CallId = new System.Windows.Forms.Button();
            this.txtDisconnect_CallId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblPhoneNumber
            // 
            resources.ApplyResources(this.lblPhoneNumber, "lblPhoneNumber");
            this.lblPhoneNumber.Name = "lblPhoneNumber";
            // 
            // txtPhoneNumber
            // 
            resources.ApplyResources(this.txtPhoneNumber, "txtPhoneNumber");
            this.txtPhoneNumber.Name = "txtPhoneNumber";
            this.txtPhoneNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPhoneNumber_KeyDown);
            this.txtPhoneNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhoneNumber_KeyPress);
            // 
            // btnDial
            // 
            resources.ApplyResources(this.btnDial, "btnDial");
            this.btnDial.Name = "btnDial";
            this.btnDial.UseVisualStyleBackColor = true;
            this.btnDial.Click += new System.EventHandler(this.btnDial_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            resources.ApplyResources(this.statusStrip1, "statusStrip1");
            this.statusStrip1.Name = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            resources.ApplyResources(this.lblStatus, "lblStatus");
            // 
            // notifyIcon1
            // 
            resources.ApplyResources(this.notifyIcon1, "notifyIcon1");
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            // 
            // btnHold
            // 
            resources.ApplyResources(this.btnHold, "btnHold");
            this.btnHold.Name = "btnHold";
            this.btnHold.UseVisualStyleBackColor = true;
            this.btnHold.Click += new System.EventHandler(this.btnHold_Click);
            // 
            // cmbStatuses
            // 
            this.cmbStatuses.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatuses.FormattingEnabled = true;
            this.cmbStatuses.Items.AddRange(new object[] {
            resources.GetString("cmbStatuses.Items"),
            resources.GetString("cmbStatuses.Items1"),
            resources.GetString("cmbStatuses.Items2"),
            resources.GetString("cmbStatuses.Items3"),
            resources.GetString("cmbStatuses.Items4"),
            resources.GetString("cmbStatuses.Items5"),
            resources.GetString("cmbStatuses.Items6"),
            resources.GetString("cmbStatuses.Items7"),
            resources.GetString("cmbStatuses.Items8"),
            resources.GetString("cmbStatuses.Items9"),
            resources.GetString("cmbStatuses.Items10"),
            resources.GetString("cmbStatuses.Items11"),
            resources.GetString("cmbStatuses.Items12"),
            resources.GetString("cmbStatuses.Items13")});
            resources.ApplyResources(this.cmbStatuses, "cmbStatuses");
            this.cmbStatuses.Name = "cmbStatuses";
            // 
            // lblStatusName
            // 
            resources.ApplyResources(this.lblStatusName, "lblStatusName");
            this.lblStatusName.Name = "lblStatusName";
            // 
            // btnChangeStatus
            // 
            resources.ApplyResources(this.btnChangeStatus, "btnChangeStatus");
            this.btnChangeStatus.Name = "btnChangeStatus";
            this.btnChangeStatus.UseVisualStyleBackColor = true;
            this.btnChangeStatus.Click += new System.EventHandler(this.btnChangeStatus_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblPhoneNumber);
            this.groupBox1.Controls.Add(this.txtPhoneNumber);
            this.groupBox1.Controls.Add(this.btnDial);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.btnHold_CallId);
            this.groupBox2.Controls.Add(this.txtHold_CallId);
            this.groupBox2.Controls.Add(this.lblHold_CallId);
            this.groupBox2.Controls.Add(this.btnHold);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // btnHold_CallId
            // 
            resources.ApplyResources(this.btnHold_CallId, "btnHold_CallId");
            this.btnHold_CallId.Name = "btnHold_CallId";
            this.btnHold_CallId.UseVisualStyleBackColor = true;
            this.btnHold_CallId.Click += new System.EventHandler(this.btnHold_CallId_Click);
            // 
            // txtHold_CallId
            // 
            resources.ApplyResources(this.txtHold_CallId, "txtHold_CallId");
            this.txtHold_CallId.Name = "txtHold_CallId";
            this.txtHold_CallId.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHold_CallId_KeyDown);
            this.txtHold_CallId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHold_CallId_KeyPress);
            // 
            // lblHold_CallId
            // 
            resources.ApplyResources(this.lblHold_CallId, "lblHold_CallId");
            this.lblHold_CallId.Name = "lblHold_CallId";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.btnDisconnect_CallId);
            this.groupBox3.Controls.Add(this.txtDisconnect_CallId);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.button2);
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // btnDisconnect_CallId
            // 
            resources.ApplyResources(this.btnDisconnect_CallId, "btnDisconnect_CallId");
            this.btnDisconnect_CallId.Name = "btnDisconnect_CallId";
            this.btnDisconnect_CallId.UseVisualStyleBackColor = true;
            this.btnDisconnect_CallId.Click += new System.EventHandler(this.btnDisconnect_CallId_Click);
            // 
            // txtDisconnect_CallId
            // 
            resources.ApplyResources(this.txtDisconnect_CallId, "txtDisconnect_CallId");
            this.txtDisconnect_CallId.Name = "txtDisconnect_CallId";
            this.txtDisconnect_CallId.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDisconnect_CallId_KeyDown);
            this.txtDisconnect_CallId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDisconnect_CallId_KeyPress);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // button2
            // 
            resources.ApplyResources(this.button2, "button2");
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblStatusName);
            this.groupBox4.Controls.Add(this.cmbStatuses);
            this.groupBox4.Controls.Add(this.btnChangeStatus);
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Load += new System.EventHandler(this.DialForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DialForm_FormClosing);
            this.Resize += new System.EventHandler(this.DialForm_Resize);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPhoneNumber;
        private System.Windows.Forms.TextBox txtPhoneNumber;
        private System.Windows.Forms.Button btnDial;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Button btnHold;
        private System.Windows.Forms.ComboBox cmbStatuses;
        private System.Windows.Forms.Label lblStatusName;
        private System.Windows.Forms.Button btnChangeStatus;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnHold_CallId;
        private System.Windows.Forms.TextBox txtHold_CallId;
        private System.Windows.Forms.Label lblHold_CallId;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDisconnect_CallId;
        private System.Windows.Forms.TextBox txtDisconnect_CallId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox4;
    }
}

