using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using ININ.Integrations.Windows.Base;
using ININ.Integrations;
//using ININ.IceLib.People;
using NDde.Server;

namespace ININ.Integrations.RemoteCommands
{
    public partial class MainForm : Form
    {
        #region Private Properties
        private MenuItem miDial, miExit;
        private System.Windows.Forms.IDataObject iData = new DataObject();
        private Kennedy.ManagedHooks.KeyboardHook keyboardHook = null;
        private bool bControlPressed = false, bAltPressed = false, bDPressed = false, bF3Pressed = false;
        private const int WM_DRAWCLIPBOARD = 776;
        private ININ.Integrations.CallControlBase ccb = null;

        // DDE Server constants
        private MyDdeServer _ddeServer = null;
        private const string DDE_SERVER_SERVICE = "ININ.Integrations.RemoteCommands";
        private const string DDE_SERVER_TOPIC = "System";
        #endregion

        #region Initialization
        public MainForm()
        {
            InitializeComponent();
            InitializeForm();
            InitializeClientConnection();
            InitializeDDEServer();
        }
        private void InitializeForm()
        {
            using (TraceTopics.DialFromClipboardTrace.scope("InitializeForm"))
            {
                try
                {
                    // CONTEXT MENU
                    TraceTopics.DialFromClipboardTrace.note("Initializing Context Menus");
                    ContextMenu cm = new ContextMenu();
                    miDial = new MenuItem(Properties.Resources.NOT_CONNECTED_TO_CLIENT, new EventHandler(DialMenuItem_Click));
                    miDial.Enabled = false;
                    cm.MenuItems.Add(miDial);
                    miExit = new MenuItem(Properties.Resources.MENU_EXIT, new EventHandler(ExitMenuItem_Click));
                    miExit.Enabled = true;
                    cm.MenuItems.Add(miExit);
                    notifyIcon1.ContextMenu = cm;

                    // INIT KEYBOARD HOOKS
                    TraceTopics.DialFromClipboardTrace.note("Initializing Keyboard Hooks");
                    keyboardHook = new Kennedy.ManagedHooks.KeyboardHook();
                    keyboardHook.KeyboardEvent += new Kennedy.ManagedHooks.KeyboardHook.KeyboardEventHandler(keyboardHook_KeyboardEvent);
                    keyboardHook.InstallHook();

                    // INIT CLIPBOARD MONITOR
                    TraceTopics.DialFromClipboardTrace.note("Initializing Clipboard Monitoring");
                    SetClipboardViewer(this.Handle.ToInt32());

                    TraceTopics.DialFromClipboardTrace.note("Form Initialized");
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex);
                    if (MessageBox.Show(Properties.Resources.EXCEPTION_OCCURED + ":" + ex.Message + " from " + ex.Source, Properties.Resources.ERROR, MessageBoxButtons.OK, MessageBoxIcon.Error) == DialogResult.OK)
                        ExitApplication();
                }
            }
        }
        private void ConnectionUp()
        {
            try
            {
                ccb = ClientCommunicationManager.Instance.ClientCallControlBase;
                ININ.IceLib.Connection.Session session = ClientCommunicationManager.Instance.IceLibSession;
                //SetStatus(Properties.Resources.CONNECTED, Properties.Resources.CONNECTED_TO_CLIENT, ToolTipIcon.Info, Properties.Resources.Icon_OK, 1000);
                SetStatus("", "", ToolTipIcon.Info, Properties.Resources.Icon_OK, 1000);
                UpdateStatus();
            }
            catch(Exception ex)
            {
                string strTest = ex.Message;
            }
        }
        private void ConnectionDown()
        {
            SetStatus(Properties.Resources.WARNING, Properties.Resources.NOT_CONNECTED_TO_CLIENT, ToolTipIcon.Warning, Properties.Resources.Icon_NotConnected, 3000);
            UpdateStatus();
        }
        private void InitializeClientConnection()
        {
            using (TraceTopics.DialFromClipboardTrace.scope("InitializeClientConnection"))
            {
                try
                {
                    TraceTopics.DialFromClipboardTrace.note("Initializing Client Connection");
                    //SetStatus(Properties.Resources.INITIALIZING, Properties.Resources.CONNECTING_TO_CLIENT, ToolTipIcon.Info, Properties.Resources.Icon_NotConnected, 100);
                    SetStatus("", "", ToolTipIcon.Info, Properties.Resources.Icon_NotConnected, 100);
                    ClientCommunicationManager.Instance.ConnectionStateChanged += new ClientCommunicationManager.ConnectionStateChangedDelegate(Instance_ConnectionStateChanged);
                    ClientCommunicationManager.Instance.AddClient(this);
                    TraceTopics.DialFromClipboardTrace.note("Client Connection Initialized. Check connection state events in next messages.");
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.error(ex.Message);
                    SetStatus(Properties.Resources.EXCEPTION_OCCURED, ex.Message + " from " + ex.Source, ToolTipIcon.Error, Properties.Resources.Icon_Error, 2000);
                }
                finally
                {
                    TraceTopics.DialFromClipboardTrace.note("Initialization Successful");
                }
            }
        }
        private void InitializeDDEServer()
        {
            TraceTopics.DialFromClipboardTrace.note("Starting DDE Server");
            _ddeServer = new MyDdeServer(DDE_SERVER_SERVICE);
            _ddeServer.SetDialForm(this);
            _ddeServer.Register();
            TraceTopics.DialFromClipboardTrace.note("DDE Server started with Service: " + DDE_SERVER_SERVICE + ". Topic: " + DDE_SERVER_TOPIC);
        }
        #endregion

        #region Clipboard
        [DllImport("user32.dll")]
        public static extern int SetClipboardViewer(int windowHandle);
        private string GetClipboardData()
        {
            string strReturn = String.Empty;
            using (TraceTopics.DialFromClipboardTrace.scope("GetClipboardData"))
            {
                try
                {
                    TraceTopics.DialFromClipboardTrace.note("Getting data from clipboard");
                    iData = Clipboard.GetDataObject();
                    TraceTopics.DialFromClipboardTrace.note("Successfully called GetDataObject()");
                    if (iData != null && iData.GetDataPresent(DataFormats.StringFormat))
                    {
                        TraceTopics.DialFromClipboardTrace.note("GetDataObject() returned a string object");
                        strReturn = (string)iData.GetData(DataFormats.StringFormat);
                        TraceTopics.DialFromClipboardTrace.note("Found data in clipboard: " + strReturn);
                        if (strReturn.Length > 50)
                        {
                            strReturn = strReturn.Substring(0, 50);
                            TraceTopics.DialFromClipboardTrace.note("After limiting data to 50 characters: " + strReturn);
                        }
                    }
                    else
                    {
                        TraceTopics.DialFromClipboardTrace.note("Clipboard data is not a string");
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex);
                    SetStatus(Properties.Resources.EXCEPTION_OCCURED, ex.Message, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                }
                finally
                {
                    TraceTopics.DialFromClipboardTrace.note("Returning: " + strReturn);
                }
            }
            return strReturn;
        }
        protected override void WndProc(ref Message m)
        {
            try
            {
                base.WndProc(ref m);
                if (m.Msg == WM_DRAWCLIPBOARD)
                    UpdateStatus();
            }
            catch (Exception ex)
            {
                using (TraceTopics.DialFromClipboardTrace.scope("WndProc"))
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex);
                    SetStatus(Properties.Resources.EXCEPTION_OCCURED, ex.Message, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                }
            }
        }
        private bool AnyValidDataInClipboard()
        {
            string strData = GetClipboardData();
            if (strData != null && strData.Length > 0)
                return true;
            else
                return false;
        }
        #endregion

        #region Keyboard Hooks
        void keyboardHook_KeyboardEvent(Kennedy.ManagedHooks.KeyboardEvents kEvent, Keys key)
        {
            if (key == Keys.Control)
            {
                if (kEvent == Kennedy.ManagedHooks.KeyboardEvents.KeyDown || kEvent == Kennedy.ManagedHooks.KeyboardEvents.SystemKeyDown)
                    bControlPressed = true;
                else
                    bControlPressed = false;
            }
            else if (key == Keys.Alt)
            {
                if (kEvent == Kennedy.ManagedHooks.KeyboardEvents.KeyDown || kEvent == Kennedy.ManagedHooks.KeyboardEvents.SystemKeyDown)
                    bAltPressed = true;
                else
                    bAltPressed = false;
            }
            else if (key == Keys.D)
            {
                if (kEvent == Kennedy.ManagedHooks.KeyboardEvents.KeyDown)
                    bDPressed = true;
                else
                    bDPressed = false;
            }
            else if (key == Keys.G)
            {
                if (kEvent == Kennedy.ManagedHooks.KeyboardEvents.KeyDown)
                    bF3Pressed = true;
                else
                    bF3Pressed = false;
            }
            if (bControlPressed && bAltPressed && (bDPressed || bF3Pressed))
            {
                using (TraceTopics.DialFromClipboardTrace.scope("keyboardHook_KeyboardEvent"))
                {
                    try
                    {
                        TraceTopics.DialFromClipboardTrace.note("CTRL+ALT+D or CTRL+ALT+F3 has been detected. Uninstalling hook to prevent repeats");
                        keyboardHook.UninstallHook();
                        TraceTopics.DialFromClipboardTrace.note("Calling DialNumber()");
                        DialNumber(bDPressed); // IF D Pressed, dial from clipboard
                        TraceTopics.DialFromClipboardTrace.note("Sleeping: Start.");
                        Thread.Sleep(1000);
                        TraceTopics.DialFromClipboardTrace.note("Sleeping: Done. Resetting control variables.");
                        bControlPressed = false;
                        bAltPressed = false;
                        bDPressed = false;
                        bF3Pressed = false;
                        TraceTopics.DialFromClipboardTrace.note("Re-installing keyboard hook.");
                        keyboardHook.InstallHook();
                    }
                    catch (Exception ex)
                    {
                        TraceTopics.DialFromClipboardTrace.exception(ex);
                        SetStatus(Properties.Resources.EXCEPTION_OCCURED, ex.Message, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                    }
                }
            }
        }
        #endregion

        #region ININ Methods (Connection and Dialing)
        private bool IsConnectedToClient()
        {
            if (ClientCommunicationManager.Instance != null && ClientCommunicationManager.Instance.ConnectedToClient)
                return true;
            else
                return false;
        }
        void Instance_ConnectionStateChanged(bool NewState)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("Instance_ConnectionStateChanged"))
            {
                TraceTopics.DialFromClipboardTrace.note("New Connection State detected: " + NewState.ToString());
                if (NewState != false)
                    ConnectionUp();
                else
                    ConnectionDown();
            }
        }
        private void DialNumber(bool bUseClipboard)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("DialNumber"))
            {
                try
                {
                    string sNumber = String.Empty;
                    if (bUseClipboard)
                    {
                        TraceTopics.DialFromClipboardTrace.note("Getting data from clipboard");
                        sNumber = GetClipboardData();
                    }
                    else
                    {
                        TraceTopics.DialFromClipboardTrace.note("Getting data from focused application");
                        SelectedText st = new SelectedText();
                        sNumber = st.GetSelectedText();
                    }

                    if (sNumber != null && sNumber.Length > 0)
                        DialNumber(sNumber);
                    else
                        TraceTopics.DialFromClipboardTrace.warning("No text to dial was found.");
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex);
                    SetStatus(Properties.Resources.EXCEPTION_OCCURED, ex.Message, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                }
            }
        }
        private void DialNumber(string sNumber)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("DialNumber(" + sNumber + ")"))
            {
                try
                {
                    if (IsConnectedToClient())
                    {

                        if (ccb != null)
                        {
                            MakeCallAction mca = new MakeCallAction(ccb);
                            mca.NumberToDial = sNumber;
                            TraceTopics.DialFromClipboardTrace.note("Calling: " + sNumber);
                            SetStatus(Properties.Resources.DIALING, sNumber, ToolTipIcon.Info, Properties.Resources.Icon_OK, 1000);
                            mca.PerformAction();
                        }
                        else
                        {
                            TraceTopics.DialFromClipboardTrace.error("CallControlBase is null. Cannot dial");
                            SetStatus(Properties.Resources.ERROR, String.Format("{0}: {1}", Properties.Resources.CANT_DIAL, Properties.Resources.NOT_CONNECTED_TO_CLIENT), ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                        }

                    }
                    else
                    {
                        SetStatus(Properties.Resources.CANT_DIAL, Properties.Resources.NOT_CONNECTED_TO_CLIENT, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                    }
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex, "Exception in {0}", "DialNumber");
                    SetStatus(Properties.Resources.EXCEPTION_OCCURED, ex.Message, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                }
            }
        }
        private void HoldCall()
        {
            using (TraceTopics.DialFromClipboardTrace.scope("HoldCall"))
            {
                try
                {
                    if (IsConnectedToClient())
                    {
                        ININ.Integrations.CallControlBase ccb = ClientCommunicationManager.Instance.ClientCallControlBase;
                        if (ccb != null)
                        {
                            ININ.ThinClient.Interaction _currentInt = ccb.CurrentInteraction();
                            //SetStatus(Properties.Resources.HOLDING, _currentInt.ID.ToString(), ToolTipIcon.Info, Properties.Resources.Icon_OK, 1000);
                            SetStatus("", "", ToolTipIcon.Info, Properties.Resources.Icon_OK, 1000);
                            ClientCommunicationManager.Instance.HoldCall(_currentInt.ID.ToString());
                        }
                        else
                        {
                            TraceTopics.DialFromClipboardTrace.error("CallControlBase is null. Can't process action.");
                            SetStatus(Properties.Resources.ERROR, String.Format("{0}: {1}", Properties.Resources.CANT_PROCESS_ACTION, Properties.Resources.NOT_CONNECTED_TO_CLIENT), ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                        }

                    }
                    else
                    {
                        SetStatus(Properties.Resources.CANT_PROCESS_ACTION, Properties.Resources.NOT_CONNECTED_TO_CLIENT, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                    }
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex, "Exception in {0}", "HoldCall");
                    SetStatus(Properties.Resources.EXCEPTION_OCCURED, ex.Message, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                }
            }
        }
        private void HoldCall(string strCallId)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("HoldCall(" + strCallId + ")"))
            {
                try
                {
                    if (IsConnectedToClient())
                    {
                        ININ.Integrations.CallControlBase ccb = ClientCommunicationManager.Instance.ClientCallControlBase;
                        if (ccb != null)
                        {
                            //SetStatus(Properties.Resources.HOLDING, strCallId, ToolTipIcon.Info, Properties.Resources.Icon_OK, 1000);
                            SetStatus("", "", ToolTipIcon.Info, Properties.Resources.Icon_OK, 1000);
                            ClientCommunicationManager.Instance.HoldCall(strCallId);
                        }
                        else
                        {
                            TraceTopics.DialFromClipboardTrace.error("CallControlBase is null. Can't process action.");
                            SetStatus(Properties.Resources.ERROR, String.Format("{0}: {1}", Properties.Resources.CANT_PROCESS_ACTION, Properties.Resources.NOT_CONNECTED_TO_CLIENT), ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                        }

                    }
                    else
                    {
                        SetStatus(Properties.Resources.CANT_PROCESS_ACTION, Properties.Resources.NOT_CONNECTED_TO_CLIENT, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                    }
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex, "Exception in {0}", "HoldCall(" + strCallId + ")");
                    SetStatus(Properties.Resources.EXCEPTION_OCCURED, ex.Message, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                }
            }
        }
        private void DisconnectCall()
        {
            using (TraceTopics.DialFromClipboardTrace.scope("DisconnectCall"))
            {
                try
                {
                    if (IsConnectedToClient())
                    {
                        ININ.Integrations.CallControlBase ccb = ClientCommunicationManager.Instance.ClientCallControlBase;
                        if (ccb != null)
                        {
                            ININ.ThinClient.Interaction _currentInt = ccb.CurrentInteraction();
                            //SetStatus(Properties.Resources.DISCONNECTING, _currentInt.ID.ToString(), ToolTipIcon.Info, Properties.Resources.Icon_OK, 1000);
                            SetStatus("", "", ToolTipIcon.Info, Properties.Resources.Icon_OK, 1000);
                            ClientCommunicationManager.Instance.DisconnectCall(_currentInt.ID.ToString());
                        }
                        else
                        {
                            TraceTopics.DialFromClipboardTrace.error("CallControlBase is null. Can't process action.");
                            SetStatus(Properties.Resources.ERROR, String.Format("{0}: {1}", Properties.Resources.CANT_PROCESS_ACTION, Properties.Resources.NOT_CONNECTED_TO_CLIENT), ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                        }

                    }
                    else
                    {
                        SetStatus(Properties.Resources.CANT_PROCESS_ACTION, Properties.Resources.NOT_CONNECTED_TO_CLIENT, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                    }
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex, "Exception in {0}", "DisconnectCall");
                    SetStatus(Properties.Resources.EXCEPTION_OCCURED, ex.Message, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                }
            }
        }
        private void DisconnectCall(string strCallId)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("DisconnectCall(" + strCallId + ")"))
            {
                try
                {
                    if (IsConnectedToClient())
                    {
                        ININ.Integrations.CallControlBase ccb = ClientCommunicationManager.Instance.ClientCallControlBase;
                        if (ccb != null)
                        {
                            SetStatus(Properties.Resources.DISCONNECTING, strCallId, ToolTipIcon.Info, Properties.Resources.Icon_OK, 1000);
                            ClientCommunicationManager.Instance.DisconnectCall(strCallId);
                        }
                        else
                        {
                            TraceTopics.DialFromClipboardTrace.error("CallControlBase is null. Can't process action.");
                            SetStatus(Properties.Resources.ERROR, String.Format("{0}: {1}", Properties.Resources.CANT_PROCESS_ACTION, Properties.Resources.NOT_CONNECTED_TO_CLIENT), ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                        }

                    }
                    else
                    {
                        SetStatus(Properties.Resources.CANT_PROCESS_ACTION, Properties.Resources.NOT_CONNECTED_TO_CLIENT, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                    }
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex, "Exception in {0}", "DisconnectCall(" + strCallId + ")");
                    SetStatus(Properties.Resources.EXCEPTION_OCCURED, ex.Message, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                }
            }
        }
        private void ChangeStatus(string strStatusName)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("ChangeStatus(" + strStatusName + ")"))
            {
                try
                {
                    if (IsConnectedToClient())
                    {
                        string UserId = ClientCommunicationManager.Instance.CurrentSession.UserId;
                        //SetStatus(Properties.Resources.CHANGING_STATUS, strStatusName, ToolTipIcon.Info, Properties.Resources.Icon_OK, 1000);
                        SetStatus("", "", ToolTipIcon.Info, Properties.Resources.Icon_OK, 1000);
                        ClientCommunicationManager.Instance.ChangeStatus(strStatusName);
                    }
                    else
                    {
                        SetStatus(Properties.Resources.CANT_PROCESS_ACTION, Properties.Resources.NOT_CONNECTED_TO_CLIENT, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                    }
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex, "Exception in {0}", "ChangeStatus(" + strStatusName + ")");
                    SetStatus(Properties.Resources.EXCEPTION_OCCURED, ex.Message, ToolTipIcon.Error, Properties.Resources.Icon_Error, 1000);
                }
            }
        }
        #endregion

        #region Form Methods
        private void SetStatus(string strTitle, string strMessage)
        {
            try
            {
                lblStatus.Text = strTitle + ": " + strMessage;
            }
            catch (Exception ex)
            {
                MessageBox.Show("SetStatus: " + ex.Message);
            }
        }
        private void SetStatus(string strTitle, string strMessage, ToolTipIcon icoTooltip, Icon icoNotify, int iTimeout)
        {
            try
            {
                if (icoNotify != null)
                    notifyIcon1.Icon = icoNotify;

                if (strTitle.Length > 0 && strMessage.Length > 0)
                {
                    lblStatus.Text = strTitle + ": " + strMessage;

                    if (icoTooltip != ToolTipIcon.None)
                    {
                        notifyIcon1.ShowBalloonTip(iTimeout, strTitle, strMessage, icoTooltip);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("SetStatus: " + ex.Message);
            }
        }
        private void UpdateStatus()
        {
            if (!IsConnectedToClient())
            {
                // UPDATE MENUS
                miDial.Text = Properties.Resources.NOT_CONNECTED_TO_CLIENT;
                miDial.Enabled = false;

                // UPDATE FORM
                //txtPhoneNumber.Text = "";
                //btnDial.Enabled = false;
            }
            else
            {
                if (AnyValidDataInClipboard())
                {
                    string strData = GetClipboardData();

                    // UPDATE MENUS
                    miDial.Text = String.Format("{0}{1}'", Properties.Resources.MENU_DIAL, strData);
                    miDial.Enabled = true;

                    // UPDATE FORM
                    txtPhoneNumber.Text = strData;
                    //btnDial.Enabled = true;
                }
                else
                {
                    // UPDATE MENUS
                    miDial.Text = Properties.Resources.MENU_NO_NUMBER_FOUND;
                    miDial.Enabled = false;

                    // UPDATE FORM
                    //txtPhoneNumber.Text = "";
                    //btnDial.Enabled = false;
                }
            }
        }
        private void btnDial_Click(object sender, EventArgs e)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("btnDial_Click"))
            {
                TraceTopics.DialFromClipboardTrace.note("Dial button has been pressed. Phone Number: " + txtPhoneNumber.Text);
                if (txtPhoneNumber.Text.Length > 0)
                    DialNumber(txtPhoneNumber.Text);
            }
        }
        private void btnHold_Click(object sender, EventArgs e)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("btnHold_Click"))
            {
                TraceTopics.DialFromClipboardTrace.note("Hold button has been pressed. Holding current call.");
                HoldCall();
            }
        }
        private void btnHold_CallId_Click(object sender, EventArgs e)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("btnHold_CallId_Click"))
            {
                TraceTopics.DialFromClipboardTrace.note("Hold button has been pressed. Holding specified call id.");
                if (txtHold_CallId.Text.Length > 0)
                {
                    HoldCall(txtHold_CallId.Text);
                }
            }
        }
        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("btnDisconnect_Click"))
            {
                TraceTopics.DialFromClipboardTrace.note("Hold button has been pressed. Disconnecting current call.");
                DisconnectCall();
            }
        }
        private void btnDisconnect_CallId_Click(object sender, EventArgs e)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("btnDisconnect_CallId_Click"))
            {
                TraceTopics.DialFromClipboardTrace.note("Disconnect button has been pressed. Disconnecting specified call id.");
                if (txtDisconnect_CallId.Text.Length > 0)
                {
                    DisconnectCall(txtDisconnect_CallId.Text);
                }
            }
        }
        private void btnChangeStatus_Click(object sender, EventArgs e)
        {
            if (cmbStatuses.Text.Length > 0)
                ChangeStatus(cmbStatuses.Text);
        }
        private void DialForm_Load(object sender, EventArgs e)
        {
            Hide();
        }
        private void DialForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            e.Cancel = true;
        }
        private void DialForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                this.Hide();
                notifyIcon1.Visible = true;
            }
        }
        private void DialMenuItem_Click(Object sender, System.EventArgs e)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("DialMenuItem_Click"))
            {
                TraceTopics.DialFromClipboardTrace.note("Dial menu item has been clicked. Calling DialNumber()");
                DialNumber(true);
            }
        }
        private void ExitMenuItem_Click(Object sender, System.EventArgs e)
        {
            using (TraceTopics.DialFromClipboardTrace.scope("ExitMenuItem_Click"))
            {
                TraceTopics.DialFromClipboardTrace.note("Exit Menu item has been clicked. Calling ExitApplication()");
                ExitApplication();
            }
        }
        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = false;

            this.Activate();
            this.Focus();
        }
        private void ExitApplication()
        {
            using (TraceTopics.DialFromClipboardTrace.scope("ExitMenuItem_Click"))
            {
                try
                {
                    TraceTopics.DialFromClipboardTrace.note("Removing client instance");
                    ClientCommunicationManager.Instance.RemoveClient(this);
                    TraceTopics.DialFromClipboardTrace.note("Disposing ClientCommunicationManager");
                    ClientCommunicationManager.Instance.Dispose();
                    if (_ddeServer != null && _ddeServer.IsRegistered)
                    {
                        TraceTopics.DialFromClipboardTrace.note("Disconnecting and unregistering DDE Server");
                        _ddeServer.Disconnect();
                        _ddeServer.Unregister();
                        TraceTopics.DialFromClipboardTrace.note("DDE Server has unregistered successfully");
                    }
                    TraceTopics.DialFromClipboardTrace.note("Disposing Main Form");
                    this.Dispose(true);
                }
                catch (Exception ex)
                {
                    TraceTopics.DialFromClipboardTrace.exception(ex);
                    MessageBox.Show("ExitApplication: " + ex.Message);
                }
            }
        }
        #region Enter Key Events
        private void txtPhoneNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                e.Handled = true;
        }
        private void txtPhoneNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (txtPhoneNumber.Text.Length > 0)
                    btnDial.PerformClick();

                e.Handled = true;
            }
        }
        private void txtHold_CallId_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                e.Handled = true;
        }
        private void txtHold_CallId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (txtPhoneNumber.Text.Length > 0)
                    btnHold_CallId.PerformClick();

                e.Handled = true;
            }
        }
        private void txtDisconnect_CallId_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                e.Handled = true;
        }
        private void txtDisconnect_CallId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (txtPhoneNumber.Text.Length > 0)
                    btnDisconnect_CallId.PerformClick();

                e.Handled = true;
            }
        }
        #endregion
        #endregion

        #region DDE Server
        private sealed class MyDdeServer : DdeServer
        {
            private MainForm dForm;
            public void SetDialForm(MainForm _dForm)
            {
                if (_dForm != null)
                    dForm = _dForm;
            }
            public MyDdeServer(string service)
                : base(service)
            {
                using (TraceTopics.DialFromClipboardTrace.scope("MyDdeServer.ctor"))
                {
                    try
                    {
                        // Nothing special to do here so far.
                    }
                    catch (Exception ex)
                    {
                        TraceTopics.DialFromClipboardTrace.exception(ex);
                    }
                }
            }
            public override void Register()
            {
                using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.MyDdeServer.Register"))
                {
                    try
                    {
                        base.Register();
                        //_Timer.Start();
                    }
                    catch (Exception ex)
                    {
                        TraceTopics.DialFromClipboardTrace.exception(ex);
                    }
                }
            }
            public override void Unregister()
            {
                using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.MyDdeServer.Unregister"))
                {
                    try
                    {
                        //_Timer.Stop();
                        base.Unregister();
                    }
                    catch (Exception ex)
                    {
                        TraceTopics.DialFromClipboardTrace.exception(ex);
                    }
                }
            }
            protected override bool OnBeforeConnect(string topic)
            {
                using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.MyDdeServer.OnBeforeConnect"))
                {
                    try
                    {
                        TraceTopics.DialFromClipboardTrace.note("OnBeforeConnect:".PadRight(16)
                            + " Service='" + base.Service + "'"
                            + " Topic='" + topic + "'");
                    }
                    catch (Exception ex)
                    {
                        TraceTopics.DialFromClipboardTrace.exception(ex);
                    }
                }
                return true;
            }
            protected override void OnAfterConnect(DdeConversation conversation)
            {
                using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.MyDdeServer.OnAfterConnect"))
                {
                    try
                    {
                        TraceTopics.DialFromClipboardTrace.note("OnAfterConnect:".PadRight(16)
                            + " Service='" + conversation.Service + "'"
                            + " Topic='" + conversation.Topic + "'"
                            + " Handle=" + conversation.Handle.ToString());
                    }
                    catch (Exception ex)
                    {
                        TraceTopics.DialFromClipboardTrace.exception(ex);
                    }
                }
            }
            protected override void OnDisconnect(DdeConversation conversation)
            {
                using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.MyDdeServer.OnDisconnect"))
                {
                    try
                    {
                        TraceTopics.DialFromClipboardTrace.note("OnDisconnect:".PadRight(16)
                            + " Service='" + conversation.Service + "'"
                            + " Topic='" + conversation.Topic + "'"
                            + " Handle=" + conversation.Handle.ToString());
                    }
                    catch (Exception ex)
                    {
                        TraceTopics.DialFromClipboardTrace.exception(ex);
                    }
                }
            }
            protected override bool OnStartAdvise(DdeConversation conversation, string item, int format)
            {
                using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.MyDdeServer.OnStartAdvise"))
                {
                    try
                    {
                        TraceTopics.DialFromClipboardTrace.note("OnStartAdvise:".PadRight(16)
                            + " Service='" + conversation.Service + "'"
                            + " Topic='" + conversation.Topic + "'"
                            + " Handle=" + conversation.Handle.ToString()
                            + " Item='" + item + "'"
                            + " Format=" + format.ToString());
                    }
                    catch (Exception ex)
                    {
                        TraceTopics.DialFromClipboardTrace.exception(ex);
                    }
                }

                // Initiate the advisory loop only if the format is CF_TEXT.
                return format == 1;
            }
            protected override void OnStopAdvise(DdeConversation conversation, string item)
            {
                using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.MyDdeServer.OnStopAdvise"))
                {
                    try
                    {
                        TraceTopics.DialFromClipboardTrace.note("OnStopAdvise:".PadRight(16)
                            + " Service='" + conversation.Service + "'"
                            + " Topic='" + conversation.Topic + "'"
                            + " Handle=" + conversation.Handle.ToString()
                            + " Item='" + item + "'");
                    }
                    catch (Exception ex)
                    {
                        TraceTopics.DialFromClipboardTrace.exception(ex);
                    }
                }
            }
            protected override ExecuteResult OnExecute(DdeConversation conversation, string command)
            {
                using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.MyDdeServer.OnExecute"))
                {
                    try
                    {
                        TraceTopics.DialFromClipboardTrace.note("OnExecute:".PadRight(16)
                            + " Service='" + conversation.Service + "'"
                            + " Topic='" + conversation.Topic + "'"
                            + " Handle=" + conversation.Handle.ToString()
                            + " Command='" + command + "'");

                        string[] slCommands = command.Split(',');
                        string strPhoneNumber;
                        string strCallId;
                        string strStatusName;

                        switch (slCommands[0])
                        {
                            case "NETClient_Dial":
                                TraceTopics.DialFromClipboardTrace.note("Received Dial command (NETClient_Dial).");
                                strPhoneNumber = slCommands[1];
                                if (strPhoneNumber.Length > 0)
                                {
                                    TraceTopics.DialFromClipboardTrace.note("Dialing " + strPhoneNumber);
                                    dForm.DialNumber(strPhoneNumber);
                                }
                                else
                                {
                                    TraceTopics.DialFromClipboardTrace.error("No phone number was passed to the NETClient_Dial command");
                                    dForm.SetStatus("Error", Properties.Resources.NO_PHONE_NUMBER_PASSED);
                                    MessageBox.Show(Properties.Resources.NO_PHONE_NUMBER_PASSED);
                                }
                                break;
                            case "NETClient_Hold":
                                TraceTopics.DialFromClipboardTrace.note("Received Hold command (NETClient_Hold).");
                                strCallId = slCommands[1];
                                if (strCallId.Length > 0)
                                {
                                    TraceTopics.DialFromClipboardTrace.note("Holding call: " + strCallId);
                                    dForm.HoldCall(strCallId);
                                }
                                else
                                {
                                    TraceTopics.DialFromClipboardTrace.error("No call id was passed to the NETClient_Hold command");
                                    dForm.SetStatus("Error", Properties.Resources.HOLD_NO_CALLID);
                                    MessageBox.Show(Properties.Resources.HOLD_NO_CALLID);
                                }
                                break;
                            case "NETClient_Disconnect":
                                TraceTopics.DialFromClipboardTrace.note("Received Disconnect command (NETClient_Disconnect).");
                                strCallId = slCommands[1];
                                if (strCallId.Length > 0)
                                {
                                    TraceTopics.DialFromClipboardTrace.note("Disconnecting call: " + strCallId);
                                    dForm.DisconnectCall(strCallId);
                                }
                                else
                                {
                                    TraceTopics.DialFromClipboardTrace.error("No call id was passed to the NETClient_Disconnect command");
                                    dForm.SetStatus("Error", Properties.Resources.DISCONNECT_NO_CALLID);
                                    MessageBox.Show(Properties.Resources.DISCONNECT_NO_CALLID);
                                }
                                break;
                            case "NETClient_StatusChange":
                                TraceTopics.DialFromClipboardTrace.note("Received Change Status command (NETClient_StatusChange).");
                                strStatusName = slCommands[1];
                                if (strStatusName.Length > 0)
                                {
                                    TraceTopics.DialFromClipboardTrace.note("Changing status to: " + strStatusName);
                                    dForm.ChangeStatus(strStatusName);
                                }
                                else
                                {
                                    TraceTopics.DialFromClipboardTrace.error("No status was passed to the NETClient_StatusChange command");
                                    dForm.SetStatus("Error", Properties.Resources.STATUSCHANGE_NO_STATUS);
                                    MessageBox.Show(Properties.Resources.STATUSCHANGE_NO_STATUS);
                                }
                                break;
                            default:
                                TraceTopics.DialFromClipboardTrace.error("Unknown DDE command: " + slCommands[0]);
                                dForm.SetStatus(Properties.Resources.DDE_UNKNOWN_COMMAND, slCommands[0]);
                                MessageBox.Show(Properties.Resources.DDE_UNKNOWN_COMMAND + slCommands[0]);
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        TraceTopics.DialFromClipboardTrace.exception(ex);
                        dForm.SetStatus("Exception", ex.Message); 
                    }
                }
                // Tell the client that the command was processed.
                return ExecuteResult.Processed;
            }
            private void TransferCallCompleted(object sender, AsyncCompletedEventArgs e)
            {
                using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.MyDdeServer.OnExecute"))
                {
                    try
                    {
                        TraceTopics.DialFromClipboardTrace.note("Transfer complete.");
                    }
                    catch (Exception ex)
                    {
                        TraceTopics.DialFromClipboardTrace.exception(ex);
                    }
                }
            }
            protected override PokeResult OnPoke(DdeConversation conversation, string item, byte[] data, int format)
            {
                using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.MyDdeServer.OnPoke"))
                {
                    try
                    {
                        TraceTopics.DialFromClipboardTrace.note("OnPoke:".PadRight(16)
                            + " Service='" + conversation.Service + "'"
                            + " Topic='" + conversation.Topic + "'"
                            + " Handle=" + conversation.Handle.ToString()
                            + " Item='" + item + "'"
                            + " Data=" + data.Length.ToString()
                            + " Format=" + format.ToString());
                    }
                    catch (Exception ex)
                    {
                        TraceTopics.DialFromClipboardTrace.exception(ex);
                    }
                }

                // Tell the client that the data was processed.
                return PokeResult.Processed;
            }
            protected override RequestResult OnRequest(DdeConversation conversation, string item, int format)
            {
                using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.MyDdeServer.OnRequest"))
                {
                    try
                    {
                        TraceTopics.DialFromClipboardTrace.note("OnRequest:".PadRight(16)
                            + " Service='" + conversation.Service + "'"
                            + " Topic='" + conversation.Topic + "'"
                            + " Handle=" + conversation.Handle.ToString()
                            + " Item='" + item + "'"
                            + " Format=" + format.ToString());

                        string[] sCommands = item.Split('|');

                        switch (sCommands[0])
                        {
                            case "IsAnyLineActive":
                                // Find out if user is on a call or not
                                TraceTopics.DialFromClipboardTrace.note("Checking if agent is on a call or not");
                                break;
                            case "FastTransfer":
                                // Transfer call to passed number
                                TraceTopics.DialFromClipboardTrace.note("Transferring call to: " + sCommands[1]);
                                break;
                            case "GetActiveCallData":
                                string strAttributeName = sCommands[1];
                                string strAttributeValue = String.Empty;
                                TraceTopics.DialFromClipboardTrace.note("Getting value of following attribute: " + strAttributeName);
                                break;
                            default:
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        TraceTopics.DialFromClipboardTrace.exception(ex);
                    }
                }
                return RequestResult.NotProcessed;
            }
            protected override byte[] OnAdvise(string topic, string item, int format)
            {
                using (TraceTopics.DialFromClipboardTrace.scope("DialFromClipboard.MyDdeServer.OnAdvise"))
                {
                    try
                    {
                        TraceTopics.DialFromClipboardTrace.note("OnAdvise:".PadRight(16)
                            + " Service='" + this.Service + "'"
                            + " Topic='" + topic + "'"
                            + " Item='" + item + "'"
                            + " Format=" + format.ToString());

                        // Send data to the client only if the format is CF_TEXT.
                        if (format == 1)
                        {
                            return System.Text.Encoding.ASCII.GetBytes("Time=" + DateTime.Now.ToString() + "\0");
                        }
                    }
                    catch (Exception ex)
                    {
                        TraceTopics.DialFromClipboardTrace.exception(ex);
                    }
                }
                return null;
            }
        }
        #endregion
    }
}