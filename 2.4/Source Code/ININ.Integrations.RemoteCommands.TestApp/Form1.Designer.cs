﻿namespace ININ.Integrations.RemoteCommands.TestApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpSettings = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.txtDDETopic = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtDDEService = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tpDial = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label27 = new System.Windows.Forms.Label();
            this.btnDial_SendDDE = new System.Windows.Forms.Button();
            this.txtDial_PhoneNumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tpHold = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.btnHold_SendDDE = new System.Windows.Forms.Button();
            this.txtHold_CallId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tpDisconnect = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.btnDisconnect_CallId = new System.Windows.Forms.Button();
            this.txtDisconnect_CallId = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tpChangeStatus = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cmbStatusNames = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.btnChangeStatus_SendDDE = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tpSettings.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.tpDial.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tpHold.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tpDisconnect.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tpChangeStatus.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpSettings);
            this.tabControl1.Controls.Add(this.tpDial);
            this.tabControl1.Controls.Add(this.tpHold);
            this.tabControl1.Controls.Add(this.tpDisconnect);
            this.tabControl1.Controls.Add(this.tpChangeStatus);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(434, 175);
            this.tabControl1.TabIndex = 0;
            // 
            // tpSettings
            // 
            this.tpSettings.Controls.Add(this.groupBox11);
            this.tpSettings.Location = new System.Drawing.Point(4, 22);
            this.tpSettings.Name = "tpSettings";
            this.tpSettings.Size = new System.Drawing.Size(426, 149);
            this.tpSettings.TabIndex = 10;
            this.tpSettings.Text = "DDE Settings";
            this.tpSettings.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label2);
            this.groupBox11.Controls.Add(this.txtDDETopic);
            this.groupBox11.Controls.Add(this.label26);
            this.groupBox11.Controls.Add(this.txtDDEService);
            this.groupBox11.Controls.Add(this.label25);
            this.groupBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox11.Location = new System.Drawing.Point(0, 0);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(426, 149);
            this.groupBox11.TabIndex = 0;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "General Settings";
            // 
            // txtDDETopic
            // 
            this.txtDDETopic.Location = new System.Drawing.Point(86, 44);
            this.txtDDETopic.Name = "txtDDETopic";
            this.txtDDETopic.Size = new System.Drawing.Size(100, 20);
            this.txtDDETopic.TabIndex = 3;
            this.txtDDETopic.Text = "System";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(17, 47);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(63, 13);
            this.label26.TabIndex = 2;
            this.label26.Text = "DDE Topic:";
            // 
            // txtDDEService
            // 
            this.txtDDEService.Location = new System.Drawing.Point(86, 17);
            this.txtDDEService.Name = "txtDDEService";
            this.txtDDEService.Size = new System.Drawing.Size(194, 20);
            this.txtDDEService.TabIndex = 1;
            this.txtDDEService.Text = "ININ.Integrations.RemoteCommands";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(8, 20);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(72, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "DDE Service:";
            // 
            // tpDial
            // 
            this.tpDial.Controls.Add(this.groupBox1);
            this.tpDial.Location = new System.Drawing.Point(4, 22);
            this.tpDial.Name = "tpDial";
            this.tpDial.Padding = new System.Windows.Forms.Padding(3);
            this.tpDial.Size = new System.Drawing.Size(426, 149);
            this.tpDial.TabIndex = 0;
            this.tpDial.Text = "Dial";
            this.tpDial.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.btnDial_SendDDE);
            this.groupBox1.Controls.Add(this.txtDial_PhoneNumber);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(420, 143);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parameters";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(226, 48);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(165, 13);
            this.label27.TabIndex = 5;
            this.label27.Text = "NETClient_Dial,<Phone Number>";
            // 
            // btnDial_SendDDE
            // 
            this.btnDial_SendDDE.Location = new System.Drawing.Point(94, 43);
            this.btnDial_SendDDE.Name = "btnDial_SendDDE";
            this.btnDial_SendDDE.Size = new System.Drawing.Size(126, 23);
            this.btnDial_SendDDE.TabIndex = 4;
            this.btnDial_SendDDE.Text = "Send DDE Command";
            this.btnDial_SendDDE.UseVisualStyleBackColor = true;
            this.btnDial_SendDDE.Click += new System.EventHandler(this.btnDial_SendDDE_Click);
            // 
            // txtDial_PhoneNumber
            // 
            this.txtDial_PhoneNumber.Location = new System.Drawing.Point(94, 17);
            this.txtDial_PhoneNumber.Name = "txtDial_PhoneNumber";
            this.txtDial_PhoneNumber.Size = new System.Drawing.Size(126, 20);
            this.txtDial_PhoneNumber.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Phone Number:";
            // 
            // tpHold
            // 
            this.tpHold.Controls.Add(this.groupBox2);
            this.tpHold.Location = new System.Drawing.Point(4, 22);
            this.tpHold.Name = "tpHold";
            this.tpHold.Padding = new System.Windows.Forms.Padding(3);
            this.tpHold.Size = new System.Drawing.Size(426, 149);
            this.tpHold.TabIndex = 1;
            this.tpHold.Text = "Hold";
            this.tpHold.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.btnHold_SendDDE);
            this.groupBox2.Controls.Add(this.txtHold_CallId);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(420, 143);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Parameters";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(218, 50);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(127, 13);
            this.label28.TabIndex = 5;
            this.label28.Text = "NETClient_Hold,<Call Id>";
            // 
            // btnHold_SendDDE
            // 
            this.btnHold_SendDDE.Location = new System.Drawing.Point(86, 45);
            this.btnHold_SendDDE.Name = "btnHold_SendDDE";
            this.btnHold_SendDDE.Size = new System.Drawing.Size(126, 23);
            this.btnHold_SendDDE.TabIndex = 4;
            this.btnHold_SendDDE.Text = "Send DDE Command";
            this.btnHold_SendDDE.UseVisualStyleBackColor = true;
            this.btnHold_SendDDE.Click += new System.EventHandler(this.btnHold_SendDDE_Click);
            // 
            // txtHold_CallId
            // 
            this.txtHold_CallId.Location = new System.Drawing.Point(86, 19);
            this.txtHold_CallId.Name = "txtHold_CallId";
            this.txtHold_CallId.Size = new System.Drawing.Size(126, 20);
            this.txtHold_CallId.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Call Id:";
            // 
            // tpDisconnect
            // 
            this.tpDisconnect.Controls.Add(this.groupBox3);
            this.tpDisconnect.Location = new System.Drawing.Point(4, 22);
            this.tpDisconnect.Name = "tpDisconnect";
            this.tpDisconnect.Size = new System.Drawing.Size(426, 149);
            this.tpDisconnect.TabIndex = 2;
            this.tpDisconnect.Text = "Disconnect";
            this.tpDisconnect.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.btnDisconnect_CallId);
            this.groupBox3.Controls.Add(this.txtDisconnect_CallId);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(426, 149);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Parameters";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(219, 53);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(159, 13);
            this.label29.TabIndex = 6;
            this.label29.Text = "NETClient_Disconnect,<Call Id>";
            // 
            // btnDisconnect_CallId
            // 
            this.btnDisconnect_CallId.Location = new System.Drawing.Point(87, 48);
            this.btnDisconnect_CallId.Name = "btnDisconnect_CallId";
            this.btnDisconnect_CallId.Size = new System.Drawing.Size(126, 23);
            this.btnDisconnect_CallId.TabIndex = 4;
            this.btnDisconnect_CallId.Text = "Send DDE Command";
            this.btnDisconnect_CallId.UseVisualStyleBackColor = true;
            this.btnDisconnect_CallId.Click += new System.EventHandler(this.btnDisconnect_SendDDE_Click);
            // 
            // txtDisconnect_CallId
            // 
            this.txtDisconnect_CallId.Location = new System.Drawing.Point(87, 19);
            this.txtDisconnect_CallId.Name = "txtDisconnect_CallId";
            this.txtDisconnect_CallId.Size = new System.Drawing.Size(126, 20);
            this.txtDisconnect_CallId.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(42, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Call Id:";
            // 
            // tpChangeStatus
            // 
            this.tpChangeStatus.Controls.Add(this.groupBox4);
            this.tpChangeStatus.Location = new System.Drawing.Point(4, 22);
            this.tpChangeStatus.Name = "tpChangeStatus";
            this.tpChangeStatus.Size = new System.Drawing.Size(426, 149);
            this.tpChangeStatus.TabIndex = 3;
            this.tpChangeStatus.Text = "Change Status";
            this.tpChangeStatus.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cmbStatusNames);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.btnChangeStatus_SendDDE);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(0, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(426, 149);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Parameters";
            // 
            // cmbStatusNames
            // 
            this.cmbStatusNames.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatusNames.FormattingEnabled = true;
            this.cmbStatusNames.Items.AddRange(new object[] {
            "At a Training Session",
            "At Lunch",
            "Available",
            "Available, Follow-Me",
            "Available, Forward",
            "Available, No ACD",
            "Away from desk",
            "Do Not Disturb",
            "Gone Home",
            "In a Meeting",
            "On Vacation",
            "Out of the Office",
            "Out of Town",
            "Working At Home"});
            this.cmbStatusNames.Location = new System.Drawing.Point(87, 19);
            this.cmbStatusNames.Name = "cmbStatusNames";
            this.cmbStatusNames.Size = new System.Drawing.Size(121, 21);
            this.cmbStatusNames.TabIndex = 7;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(219, 51);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(204, 13);
            this.label30.TabIndex = 6;
            this.label30.Text = "NETClient_ChangeStatus,<Status Name>";
            // 
            // btnChangeStatus_SendDDE
            // 
            this.btnChangeStatus_SendDDE.Location = new System.Drawing.Point(87, 46);
            this.btnChangeStatus_SendDDE.Name = "btnChangeStatus_SendDDE";
            this.btnChangeStatus_SendDDE.Size = new System.Drawing.Size(126, 23);
            this.btnChangeStatus_SendDDE.TabIndex = 4;
            this.btnChangeStatus_SendDDE.Text = "Send DDE Command";
            this.btnChangeStatus_SendDDE.UseVisualStyleBackColor = true;
            this.btnChangeStatus_SendDDE.Click += new System.EventHandler(this.btnChangeStatus_SendDDE_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Status Name:";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 153);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(434, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(87, 71);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Send DDE Command";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(87, 44);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(126, 20);
            this.textBox5.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Call ID:";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(87, 17);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(126, 20);
            this.textBox6.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Case Number:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(19, 73);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(62, 13);
            this.label17.TabIndex = 6;
            this.label17.Text = "NM Site ID:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(87, 70);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(126, 20);
            this.textBox1.TabIndex = 5;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(87, 96);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(126, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Send DDE Command";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(87, 44);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(126, 20);
            this.textBox2.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(40, 47);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Call ID:";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(87, 17);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(126, 20);
            this.textBox3.TabIndex = 1;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(53, 20);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(28, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "ANI:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(282, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Note: These are default settings. Only changed if asked to";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 175);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "ININ.Integrations.RemoteCommands Test Application";
            this.tabControl1.ResumeLayout(false);
            this.tpSettings.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.tpDial.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tpHold.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tpDisconnect.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tpChangeStatus.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpDial;
        private System.Windows.Forms.TabPage tpHold;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtDial_PhoneNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtHold_CallId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button btnHold_SendDDE;
        private System.Windows.Forms.Button btnDial_SendDDE;
        private System.Windows.Forms.TabPage tpDisconnect;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnDisconnect_CallId;
        private System.Windows.Forms.TextBox txtDisconnect_CallId;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tpChangeStatus;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnChangeStatus_SendDDE;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TabPage tpSettings;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox txtDDETopic;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtDDEService;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox cmbStatusNames;
        private System.Windows.Forms.Label label2;
    }
}

