﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using NDde.Client;

namespace ININ.Integrations.RemoteCommands.TestApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void LogMessage(bool bPopup, bool bError, string strMessage)
        {
            toolStripStatusLabel1.Text = strMessage;
         
            if (bError)
                toolStripStatusLabel1.ForeColor = Color.Red;
            else
                toolStripStatusLabel1.ForeColor = Color.Black;

            if (bPopup)
            {
                if (bError)
                    MessageBox.Show("Error: " + strMessage);
                else
                    MessageBox.Show(strMessage);
            }
        }

        private bool SendDDE(String strCommand)
        {
            bool bResult = false;
            try
            {
                // Create the client
                //ININ.ThinClient.PlugIn.ClarifyIntegration.IntegrationMgr.TraceMessage(false, "Creating DDE client...");
                //using (DdeClient client = new DdeClient("Excel", "System")) // Test with Excel
                using (DdeClient client = new DdeClient(txtDDEService.Text, txtDDETopic.Text))
                {
                    // Subscribe to the Disconnected event.  This event will notify the application when a conversation has been terminated.
                    client.Disconnected += OnDisconnected;

                    // Connect to the server.  It must be running or an exception will be thrown.
                    //ININ.ThinClient.PlugIn.ClarifyIntegration.IntegrationMgr.TraceMessage(false, "Connecting...");
                    client.Connect();

                    // Asynchronous Execute Operation
                    //client.BeginExecute("[NEW(1)]", OnExecuteComplete, client); // Test with Excel
                    //ININ.ThinClient.PlugIn.ClarifyIntegration.IntegrationMgr.TraceMessage(false, "Sending following DDE command: " + strCommand + " to service: " + client.Service + ", topic: " + client.Topic);
                    client.BeginExecute(strCommand, OnExecuteComplete, client);
                    //ININ.ThinClient.PlugIn.ClarifyIntegration.IntegrationMgr.TraceMessage(false, "DDE command sent");
                }
            }
            catch (Exception ex)
            {
                LogMessage(true, true, ex.Message);
            }
            return bResult;
        }
        private void OnExecuteComplete(IAsyncResult ar)
        {
            try
            {
                DdeClient client = (DdeClient)ar.AsyncState;
                LogMessage(false, false, "Command Completed");
                //client.EndExecute(ar); // Throwing an exception when using "using()"
            }
            catch (Exception ex)
            {
                LogMessage(true, true, "In OnExecuteComplete: " + ex.Message);
            }
        }
        private static void OnDisconnected(object sender, DdeDisconnectedEventArgs args)
        {
            //ININ.ThinClient.PlugIn.ClarifyIntegration.IntegrationMgr.TraceMessage(false, "OnDisconnected: " + "IsServerInitiated=" + args.IsServerInitiated.ToString() + " " + "IsDisposed=" + args.IsDisposed.ToString());
        }

        private void btnDial_SendDDE_Click(object sender, EventArgs e)
        {
            string strCommand = "NETClient_Dial," + txtDial_PhoneNumber.Text;
            try
            {
                LogMessage(false, false, "Sending command: " + strCommand);
                if (SendDDE(strCommand))
                    LogMessage(true, false, "Command sent");
            }
            catch (Exception ex)
            {
                LogMessage(true, true, ex.Message);
            }
        }
        private void btnHold_SendDDE_Click(object sender, EventArgs e)
        {
            string strCommand = "NETClient_Hold," + txtHold_CallId.Text;
            try
            {
                LogMessage(false, false, "Sending following command: " + strCommand);
                if (SendDDE(strCommand))
                    LogMessage(true, false, "Command sent");
            }
            catch (Exception ex)
            {
                LogMessage(true, true, ex.Message);
            }
        }
        private void btnDisconnect_SendDDE_Click(object sender, EventArgs e)
        {
            string strCommand = "NETClient_Disconnect," + txtDisconnect_CallId.Text;
            try
            {
                LogMessage(false, false, "Sending following command: " + strCommand);
                if (SendDDE(strCommand))
                    LogMessage(true, false, "Command sent");
            }
            catch (Exception ex)
            {
                LogMessage(true, true, ex.Message);
            }
        }
        private void btnChangeStatus_SendDDE_Click(object sender, EventArgs e)
        {
            string strCommand = "NETClient_StatusChange," + cmbStatusNames.SelectedItem.ToString();
            try
            {
                if (cmbStatusNames.SelectedItem.ToString() == String.Empty)
                {
                    LogMessage(true, true, "No status was selected. Not sending command");
                }
                else
                {
                    LogMessage(false, false, "Sending following command: " + strCommand);
                    if (SendDDE(strCommand))
                        LogMessage(true, false, "Command sent");
                }
            }
            catch (Exception ex)
            {
                LogMessage(true, true, ex.Message);
            }
        }
    }
}
