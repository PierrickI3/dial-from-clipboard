﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using NDde.Client;

namespace DDE_Test_Application
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void LogMessage(bool bPopup, bool bError, string strMessage)
        {
            toolStripStatusLabel1.Text = strMessage;
         
            if (bError)
                toolStripStatusLabel1.ForeColor = Color.Red;
            else
                toolStripStatusLabel1.ForeColor = Color.Black;

            if (bPopup)
            {
                if (bError)
                    MessageBox.Show("Error: " + strMessage);
                else
                    MessageBox.Show(strMessage);
            }
        }

        private bool SendDDE(String strCommand)
        {
            bool bResult = false;
            try
            {
                // Create the client
                //ININ.ThinClient.PlugIn.ClarifyIntegration.IntegrationMgr.TraceMessage(false, "Creating DDE client...");
                //using (DdeClient client = new DdeClient("Excel", "System")) // Test with Excel
                using (DdeClient client = new DdeClient(txtDDEService.Text, txtDDETopic.Text))
                {
                    // Subscribe to the Disconnected event.  This event will notify the application when a conversation has been terminated.
                    client.Disconnected += OnDisconnected;

                    // Connect to the server.  It must be running or an exception will be thrown.
                    //ININ.ThinClient.PlugIn.ClarifyIntegration.IntegrationMgr.TraceMessage(false, "Connecting...");
                    client.Connect();

                    // Asynchronous Execute Operation
                    //client.BeginExecute("[NEW(1)]", OnExecuteComplete, client); // Test with Excel
                    //ININ.ThinClient.PlugIn.ClarifyIntegration.IntegrationMgr.TraceMessage(false, "Sending following DDE command: " + strCommand + " to service: " + client.Service + ", topic: " + client.Topic);
                    client.BeginExecute(strCommand, OnExecuteComplete, client);
                    //ININ.ThinClient.PlugIn.ClarifyIntegration.IntegrationMgr.TraceMessage(false, "DDE command sent");
                }
            }
            catch (Exception ex)
            {
                LogMessage(true, true, ex.Message);
            }
            return bResult;
        }
        private void OnExecuteComplete(IAsyncResult ar)
        {
            try
            {
                DdeClient client = (DdeClient)ar.AsyncState;
                //client.EndExecute(ar); // Throwing an exception when using "using()"
            }
            catch (Exception ex)
            {
                LogMessage(true, true, "In OnExecuteComplete: " + ex.Message);
            }
        }
        private static void OnDisconnected(object sender, DdeDisconnectedEventArgs args)
        {
            //ININ.ThinClient.PlugIn.ClarifyIntegration.IntegrationMgr.TraceMessage(false, "OnDisconnected: " + "IsServerInitiated=" + args.IsServerInitiated.ToString() + " " + "IsDisposed=" + args.IsDisposed.ToString());
        }

        private void btnShowCaseSendDDE_Click(object sender, EventArgs e)
        {
            string strCommand = "CTI_ShowCase," + txtShowCaseCaseNumber.Text + "," + txtShowCaseCallId.Text;
            try
            {
                LogMessage(false, false, "Sending following command: " + strCommand);
                if (SendDDE(strCommand))
                    LogMessage(true, false, "Command sent");
            }
            catch (Exception ex)
            {
                LogMessage(true, true, ex.Message);
            }
        }
        private void btnNPopPhoneSendDDE_Click(object sender, EventArgs e)
        {
            string strCommand = "CTI_N_PopPhone," + txtNPopPhoneANI.Text + "," + txtNPopPhoneCallId.Text;
            try
            {
                LogMessage(false, false, "Sending following command: " + strCommand);
                if (SendDDE(strCommand))
                    LogMessage(true, false, "Command sent");
            }
            catch (Exception ex)
            {
                LogMessage(true, true, ex.Message);
            }
        }
        private void btnPOPPinDirect1SendDDE_Click(object sender, EventArgs e)
        {
            string strCommand = "CTI_POP_PinDirect," + txtPOPPinDirect1CustomerPin.Text;
            try
            {
                LogMessage(false, false, "Sending following command: " + strCommand);
                if (SendDDE(strCommand))
                    LogMessage(true, false, "Command sent");
            }
            catch (Exception ex)
            {
                LogMessage(true, true, ex.Message);
            }
        }
        private void btnPopPinDirect2SendDDE_Click(object sender, EventArgs e)
        {
            string strCommand = "CTI_POP_PinDirect," + txtPOPPinDirect2ANI.Text + "," + txtPOPPinDirect2CallId.Text;
            try
            {
                LogMessage(false, false, "Sending following command: " + strCommand);
                if (SendDDE(strCommand))
                    LogMessage(true, false, "Command sent");
            }
            catch (Exception ex)
            {
                LogMessage(true, true, ex.Message);
            }
        }
        private void btnPMCCDirectSendDDE_Click(object sender, EventArgs e)
        {
            string strCommand = "CTI_POP_PMCCDirect," + txtPMCCDirectPMCCSiteID.Text;
            try
            {
                LogMessage(false, false, "Sending following command: " + strCommand);
                if (SendDDE(strCommand))
                    LogMessage(true, false, "Command sent");
            }
            catch (Exception ex)
            {
                LogMessage(true, true, ex.Message);
            }
        }
        private void btnPINNumberSendDDE_Click(object sender, EventArgs e)
        {
            string strCommand = "CTI_POP_PinNumber," + txtPINNumberANI.Text + "," + txtPINNumberCallId.Text + "," + txtPINNumberPINNumber.Text;
            try
            {
                LogMessage(false, false, "Sending following command: " + strCommand);
                if (SendDDE(strCommand))
                    LogMessage(true, false, "Command sent");
            }
            catch (Exception ex)
            {
                LogMessage(true, true, ex.Message);
            }
        }
        private void btnPopSerno1SendDDE_Click(object sender, EventArgs e)
        {
            string strCommand = "CTI_POP_Serno," + txtPopSerno1ANI.Text + "," + txtPopSerno1CallId.Text + "," + txtPopSerno1NMSiteId.Text;
            try
            {
                LogMessage(false, false, "Sending following command: " + strCommand);
                if (SendDDE(strCommand))
                    LogMessage(true, false, "Command sent");
            }
            catch (Exception ex)
            {
                LogMessage(true, true, ex.Message);
            }
        }
        private void btnPopSerno2SendDDE_Click(object sender, EventArgs e)
        {
            string strCommand = "CTI_POP_Serno," + txtPopSerno2ANI.Text + "," + txtPopSerno2CallId.Text + "," + txtPopSerno2SerialNumber.Text;
            try
            {
                LogMessage(false, false, "Sending following command: " + strCommand);
                if (SendDDE(strCommand))
                    LogMessage(true, false, "Command sent");
            }
            catch (Exception ex)
            {
                LogMessage(true, true, ex.Message);
            }
        }
        private void btnCheckCallSendDDE_Click(object sender, EventArgs e)
        {
            string strCommand = "CTI_CheckCall," + txtCheckCallCallId.Text;
            try
            {
                LogMessage(false, false, "Sending following command: " + strCommand);
                if (SendDDE(strCommand))
                    LogMessage(true, false, "Command sent");
            }
            catch (Exception ex)
            {
                LogMessage(true, true, ex.Message);
            }
        }
        private void btnAcceptCaseSendDDE_Click(object sender, EventArgs e)
        {
            string strCommand = "CTI_AcceptCase," + txtAcceptCaseCaseNumber.Text;
            try
            {
                LogMessage(false, false, "Sending following command: " + strCommand);
                if (SendDDE(strCommand))
                    LogMessage(true, false, "Command sent");
            }
            catch (Exception ex)
            {
                LogMessage(true, true, ex.Message);
            }
        }
    }
}
