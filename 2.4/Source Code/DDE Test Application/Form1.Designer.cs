﻿namespace DDE_Test_Application
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpSettings = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.txtDDETopic = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtDDEService = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tpShowCase = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label27 = new System.Windows.Forms.Label();
            this.btnShowCaseSendDDE = new System.Windows.Forms.Button();
            this.txtShowCaseCallId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtShowCaseCaseNumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tpNPopPhone = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.btnNPopPhoneSendDDE = new System.Windows.Forms.Button();
            this.txtNPopPhoneCallId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNPopPhoneANI = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tpPOPPinDirect1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.btnPOPPinDirect1SendDDE = new System.Windows.Forms.Button();
            this.txtPOPPinDirect1CustomerPin = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tpPOPPinDirect2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.btnPopPinDirect2SendDDE = new System.Windows.Forms.Button();
            this.txtPOPPinDirect2CallId = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPOPPinDirect2ANI = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tpPOPPMCCDirect = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label31 = new System.Windows.Forms.Label();
            this.btnPMCCDirectSendDDE = new System.Windows.Forms.Button();
            this.txtPMCCDirectPMCCSiteID = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tpPOPPinNumber = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPINNumberPINNumber = new System.Windows.Forms.TextBox();
            this.btnPINNumberSendDDE = new System.Windows.Forms.Button();
            this.txtPINNumberCallId = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPINNumberANI = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tpPOPSerno1 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtPopSerno1NMSiteId = new System.Windows.Forms.TextBox();
            this.btnPopSerno1SendDDE = new System.Windows.Forms.Button();
            this.txtPopSerno1CallId = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPopSerno1ANI = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tpPOPSerno2 = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtPopSerno2SerialNumber = new System.Windows.Forms.TextBox();
            this.btnPopSerno2SendDDE = new System.Windows.Forms.Button();
            this.txtPopSerno2CallId = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtPopSerno2ANI = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tpCheckCall = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.btnCheckCallSendDDE = new System.Windows.Forms.Button();
            this.txtCheckCallCallId = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tpAcceptCase = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label36 = new System.Windows.Forms.Label();
            this.btnAcceptCaseSendDDE = new System.Windows.Forms.Button();
            this.txtAcceptCaseCaseNumber = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tpSettings.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.tpShowCase.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tpNPopPhone.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tpPOPPinDirect1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tpPOPPinDirect2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tpPOPPMCCDirect.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tpPOPPinNumber.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tpPOPSerno1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.tpPOPSerno2.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tpCheckCall.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.tpAcceptCase.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpSettings);
            this.tabControl1.Controls.Add(this.tpShowCase);
            this.tabControl1.Controls.Add(this.tpNPopPhone);
            this.tabControl1.Controls.Add(this.tpPOPPinDirect1);
            this.tabControl1.Controls.Add(this.tpPOPPinDirect2);
            this.tabControl1.Controls.Add(this.tpPOPPMCCDirect);
            this.tabControl1.Controls.Add(this.tpPOPPinNumber);
            this.tabControl1.Controls.Add(this.tpPOPSerno1);
            this.tabControl1.Controls.Add(this.tpPOPSerno2);
            this.tabControl1.Controls.Add(this.tpCheckCall);
            this.tabControl1.Controls.Add(this.tpAcceptCase);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(573, 175);
            this.tabControl1.TabIndex = 0;
            // 
            // tpSettings
            // 
            this.tpSettings.Controls.Add(this.groupBox11);
            this.tpSettings.Location = new System.Drawing.Point(4, 22);
            this.tpSettings.Name = "tpSettings";
            this.tpSettings.Size = new System.Drawing.Size(565, 149);
            this.tpSettings.TabIndex = 10;
            this.tpSettings.Text = "Settings";
            this.tpSettings.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.txtDDETopic);
            this.groupBox11.Controls.Add(this.label26);
            this.groupBox11.Controls.Add(this.txtDDEService);
            this.groupBox11.Controls.Add(this.label25);
            this.groupBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox11.Location = new System.Drawing.Point(0, 0);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(565, 149);
            this.groupBox11.TabIndex = 0;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "General Settings";
            // 
            // txtDDETopic
            // 
            this.txtDDETopic.Location = new System.Drawing.Point(86, 44);
            this.txtDDETopic.Name = "txtDDETopic";
            this.txtDDETopic.Size = new System.Drawing.Size(100, 20);
            this.txtDDETopic.TabIndex = 3;
            this.txtDDETopic.Text = "Clarify";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(17, 47);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(63, 13);
            this.label26.TabIndex = 2;
            this.label26.Text = "DDE Topic:";
            // 
            // txtDDEService
            // 
            this.txtDDEService.Location = new System.Drawing.Point(86, 17);
            this.txtDDEService.Name = "txtDDEService";
            this.txtDDEService.Size = new System.Drawing.Size(100, 20);
            this.txtDDEService.TabIndex = 1;
            this.txtDDEService.Text = "ClarifyApp";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(8, 20);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(72, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "DDE Service:";
            // 
            // tpShowCase
            // 
            this.tpShowCase.Controls.Add(this.groupBox1);
            this.tpShowCase.Location = new System.Drawing.Point(4, 22);
            this.tpShowCase.Name = "tpShowCase";
            this.tpShowCase.Padding = new System.Windows.Forms.Padding(3);
            this.tpShowCase.Size = new System.Drawing.Size(565, 149);
            this.tpShowCase.TabIndex = 0;
            this.tpShowCase.Text = "CTI_ShowCase";
            this.tpShowCase.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.btnShowCaseSendDDE);
            this.groupBox1.Controls.Add(this.txtShowCaseCallId);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtShowCaseCaseNumber);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(559, 143);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parameters";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(219, 76);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(204, 13);
            this.label27.TabIndex = 5;
            this.label27.Text = "CTI_ShowCase,<Case Number>,<Call Id>";
            // 
            // btnShowCaseSendDDE
            // 
            this.btnShowCaseSendDDE.Location = new System.Drawing.Point(87, 71);
            this.btnShowCaseSendDDE.Name = "btnShowCaseSendDDE";
            this.btnShowCaseSendDDE.Size = new System.Drawing.Size(126, 23);
            this.btnShowCaseSendDDE.TabIndex = 4;
            this.btnShowCaseSendDDE.Text = "Send DDE Command";
            this.btnShowCaseSendDDE.UseVisualStyleBackColor = true;
            this.btnShowCaseSendDDE.Click += new System.EventHandler(this.btnShowCaseSendDDE_Click);
            // 
            // txtShowCaseCallId
            // 
            this.txtShowCaseCallId.Location = new System.Drawing.Point(87, 44);
            this.txtShowCaseCallId.Name = "txtShowCaseCallId";
            this.txtShowCaseCallId.Size = new System.Drawing.Size(126, 20);
            this.txtShowCaseCallId.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Call Id:";
            // 
            // txtShowCaseCaseNumber
            // 
            this.txtShowCaseCaseNumber.Location = new System.Drawing.Point(87, 17);
            this.txtShowCaseCaseNumber.Name = "txtShowCaseCaseNumber";
            this.txtShowCaseCaseNumber.Size = new System.Drawing.Size(126, 20);
            this.txtShowCaseCaseNumber.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Case Number:";
            // 
            // tpNPopPhone
            // 
            this.tpNPopPhone.Controls.Add(this.groupBox2);
            this.tpNPopPhone.Location = new System.Drawing.Point(4, 22);
            this.tpNPopPhone.Name = "tpNPopPhone";
            this.tpNPopPhone.Padding = new System.Windows.Forms.Padding(3);
            this.tpNPopPhone.Size = new System.Drawing.Size(565, 149);
            this.tpNPopPhone.TabIndex = 1;
            this.tpNPopPhone.Text = "CTI_N_PopPhone";
            this.tpNPopPhone.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.btnNPopPhoneSendDDE);
            this.groupBox2.Controls.Add(this.txtNPopPhoneCallId);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtNPopPhoneANI);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(559, 143);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Parameters";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(219, 76);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(171, 13);
            this.label28.TabIndex = 5;
            this.label28.Text = "CTI_N_PopPhone,<ANI>,<Call Id>";
            // 
            // btnNPopPhoneSendDDE
            // 
            this.btnNPopPhoneSendDDE.Location = new System.Drawing.Point(87, 71);
            this.btnNPopPhoneSendDDE.Name = "btnNPopPhoneSendDDE";
            this.btnNPopPhoneSendDDE.Size = new System.Drawing.Size(126, 23);
            this.btnNPopPhoneSendDDE.TabIndex = 4;
            this.btnNPopPhoneSendDDE.Text = "Send DDE Command";
            this.btnNPopPhoneSendDDE.UseVisualStyleBackColor = true;
            this.btnNPopPhoneSendDDE.Click += new System.EventHandler(this.btnNPopPhoneSendDDE_Click);
            // 
            // txtNPopPhoneCallId
            // 
            this.txtNPopPhoneCallId.Location = new System.Drawing.Point(87, 44);
            this.txtNPopPhoneCallId.Name = "txtNPopPhoneCallId";
            this.txtNPopPhoneCallId.Size = new System.Drawing.Size(126, 20);
            this.txtNPopPhoneCallId.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Call Id:";
            // 
            // txtNPopPhoneANI
            // 
            this.txtNPopPhoneANI.Location = new System.Drawing.Point(87, 17);
            this.txtNPopPhoneANI.Name = "txtNPopPhoneANI";
            this.txtNPopPhoneANI.Size = new System.Drawing.Size(126, 20);
            this.txtNPopPhoneANI.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(53, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "ANI:";
            // 
            // tpPOPPinDirect1
            // 
            this.tpPOPPinDirect1.Controls.Add(this.groupBox3);
            this.tpPOPPinDirect1.Location = new System.Drawing.Point(4, 22);
            this.tpPOPPinDirect1.Name = "tpPOPPinDirect1";
            this.tpPOPPinDirect1.Size = new System.Drawing.Size(565, 149);
            this.tpPOPPinDirect1.TabIndex = 2;
            this.tpPOPPinDirect1.Text = "CTI_POP_PinDirect (#1)";
            this.tpPOPPinDirect1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.btnPOPPinDirect1SendDDE);
            this.groupBox3.Controls.Add(this.txtPOPPinDirect1CustomerPin);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(565, 149);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Parameters";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(219, 48);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(178, 13);
            this.label29.TabIndex = 6;
            this.label29.Text = "CTI_POP_PinDirect,<Customer Pin>";
            // 
            // btnPOPPinDirect1SendDDE
            // 
            this.btnPOPPinDirect1SendDDE.Location = new System.Drawing.Point(87, 43);
            this.btnPOPPinDirect1SendDDE.Name = "btnPOPPinDirect1SendDDE";
            this.btnPOPPinDirect1SendDDE.Size = new System.Drawing.Size(126, 23);
            this.btnPOPPinDirect1SendDDE.TabIndex = 4;
            this.btnPOPPinDirect1SendDDE.Text = "Send DDE Command";
            this.btnPOPPinDirect1SendDDE.UseVisualStyleBackColor = true;
            this.btnPOPPinDirect1SendDDE.Click += new System.EventHandler(this.btnPOPPinDirect1SendDDE_Click);
            // 
            // txtPOPPinDirect1CustomerPin
            // 
            this.txtPOPPinDirect1CustomerPin.Location = new System.Drawing.Point(87, 17);
            this.txtPOPPinDirect1CustomerPin.Name = "txtPOPPinDirect1CustomerPin";
            this.txtPOPPinDirect1CustomerPin.Size = new System.Drawing.Size(126, 20);
            this.txtPOPPinDirect1CustomerPin.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Customer Pin:";
            // 
            // tpPOPPinDirect2
            // 
            this.tpPOPPinDirect2.Controls.Add(this.groupBox4);
            this.tpPOPPinDirect2.Location = new System.Drawing.Point(4, 22);
            this.tpPOPPinDirect2.Name = "tpPOPPinDirect2";
            this.tpPOPPinDirect2.Size = new System.Drawing.Size(565, 149);
            this.tpPOPPinDirect2.TabIndex = 3;
            this.tpPOPPinDirect2.Text = "CTI_POP_PinDirect (#2)";
            this.tpPOPPinDirect2.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.btnPopPinDirect2SendDDE);
            this.groupBox4.Controls.Add(this.txtPOPPinDirect2CallId);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.txtPOPPinDirect2ANI);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(0, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(565, 149);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Parameters";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(219, 76);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(178, 13);
            this.label30.TabIndex = 6;
            this.label30.Text = "CTI_POP_PinDirect,<ANI>,<Call Id>";
            // 
            // btnPopPinDirect2SendDDE
            // 
            this.btnPopPinDirect2SendDDE.Location = new System.Drawing.Point(87, 71);
            this.btnPopPinDirect2SendDDE.Name = "btnPopPinDirect2SendDDE";
            this.btnPopPinDirect2SendDDE.Size = new System.Drawing.Size(126, 23);
            this.btnPopPinDirect2SendDDE.TabIndex = 4;
            this.btnPopPinDirect2SendDDE.Text = "Send DDE Command";
            this.btnPopPinDirect2SendDDE.UseVisualStyleBackColor = true;
            this.btnPopPinDirect2SendDDE.Click += new System.EventHandler(this.btnPopPinDirect2SendDDE_Click);
            // 
            // txtPOPPinDirect2CallId
            // 
            this.txtPOPPinDirect2CallId.Location = new System.Drawing.Point(87, 44);
            this.txtPOPPinDirect2CallId.Name = "txtPOPPinDirect2CallId";
            this.txtPOPPinDirect2CallId.Size = new System.Drawing.Size(126, 20);
            this.txtPOPPinDirect2CallId.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(40, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Call Id:";
            // 
            // txtPOPPinDirect2ANI
            // 
            this.txtPOPPinDirect2ANI.Location = new System.Drawing.Point(87, 17);
            this.txtPOPPinDirect2ANI.Name = "txtPOPPinDirect2ANI";
            this.txtPOPPinDirect2ANI.Size = new System.Drawing.Size(126, 20);
            this.txtPOPPinDirect2ANI.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(53, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "ANI:";
            // 
            // tpPOPPMCCDirect
            // 
            this.tpPOPPMCCDirect.Controls.Add(this.groupBox5);
            this.tpPOPPMCCDirect.Location = new System.Drawing.Point(4, 22);
            this.tpPOPPMCCDirect.Name = "tpPOPPMCCDirect";
            this.tpPOPPMCCDirect.Size = new System.Drawing.Size(565, 149);
            this.tpPOPPMCCDirect.TabIndex = 4;
            this.tpPOPPMCCDirect.Text = "CTI_POP_PMCC_Direct";
            this.tpPOPPMCCDirect.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label31);
            this.groupBox5.Controls.Add(this.btnPMCCDirectSendDDE);
            this.groupBox5.Controls.Add(this.txtPMCCDirectPMCCSiteID);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(0, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(565, 149);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Parameters";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(219, 48);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(200, 13);
            this.label31.TabIndex = 6;
            this.label31.Text = "CTI_POP_PMCC_Direct,<PMCC Site Id>";
            // 
            // btnPMCCDirectSendDDE
            // 
            this.btnPMCCDirectSendDDE.Location = new System.Drawing.Point(87, 43);
            this.btnPMCCDirectSendDDE.Name = "btnPMCCDirectSendDDE";
            this.btnPMCCDirectSendDDE.Size = new System.Drawing.Size(126, 23);
            this.btnPMCCDirectSendDDE.TabIndex = 4;
            this.btnPMCCDirectSendDDE.Text = "Send DDE Command";
            this.btnPMCCDirectSendDDE.UseVisualStyleBackColor = true;
            this.btnPMCCDirectSendDDE.Click += new System.EventHandler(this.btnPMCCDirectSendDDE_Click);
            // 
            // txtPMCCDirectPMCCSiteID
            // 
            this.txtPMCCDirectPMCCSiteID.Location = new System.Drawing.Point(87, 17);
            this.txtPMCCDirectPMCCSiteID.Name = "txtPMCCDirectPMCCSiteID";
            this.txtPMCCDirectPMCCSiteID.Size = new System.Drawing.Size(126, 20);
            this.txtPMCCDirectPMCCSiteID.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "PMCC Site Id:";
            // 
            // tpPOPPinNumber
            // 
            this.tpPOPPinNumber.Controls.Add(this.groupBox6);
            this.tpPOPPinNumber.Location = new System.Drawing.Point(4, 22);
            this.tpPOPPinNumber.Name = "tpPOPPinNumber";
            this.tpPOPPinNumber.Size = new System.Drawing.Size(565, 149);
            this.tpPOPPinNumber.TabIndex = 5;
            this.tpPOPPinNumber.Text = "CTI_POP_PinNumber";
            this.tpPOPPinNumber.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label32);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.txtPINNumberPINNumber);
            this.groupBox6.Controls.Add(this.btnPINNumberSendDDE);
            this.groupBox6.Controls.Add(this.txtPINNumberCallId);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.txtPINNumberANI);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Location = new System.Drawing.Point(0, 0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(565, 149);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Parameters";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(219, 101);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(257, 13);
            this.label32.TabIndex = 7;
            this.label32.Text = "CTI_POP_PinNumber,<ANI>,<Call Id>,<Pin Number>";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 73);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Pin Number:";
            // 
            // txtPINNumberPINNumber
            // 
            this.txtPINNumberPINNumber.Location = new System.Drawing.Point(87, 70);
            this.txtPINNumberPINNumber.Name = "txtPINNumberPINNumber";
            this.txtPINNumberPINNumber.Size = new System.Drawing.Size(126, 20);
            this.txtPINNumberPINNumber.TabIndex = 5;
            // 
            // btnPINNumberSendDDE
            // 
            this.btnPINNumberSendDDE.Location = new System.Drawing.Point(87, 96);
            this.btnPINNumberSendDDE.Name = "btnPINNumberSendDDE";
            this.btnPINNumberSendDDE.Size = new System.Drawing.Size(126, 23);
            this.btnPINNumberSendDDE.TabIndex = 4;
            this.btnPINNumberSendDDE.Text = "Send DDE Command";
            this.btnPINNumberSendDDE.UseVisualStyleBackColor = true;
            this.btnPINNumberSendDDE.Click += new System.EventHandler(this.btnPINNumberSendDDE_Click);
            // 
            // txtPINNumberCallId
            // 
            this.txtPINNumberCallId.Location = new System.Drawing.Point(87, 44);
            this.txtPINNumberCallId.Name = "txtPINNumberCallId";
            this.txtPINNumberCallId.Size = new System.Drawing.Size(126, 20);
            this.txtPINNumberCallId.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(40, 47);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Call Id:";
            // 
            // txtPINNumberANI
            // 
            this.txtPINNumberANI.Location = new System.Drawing.Point(87, 17);
            this.txtPINNumberANI.Name = "txtPINNumberANI";
            this.txtPINNumberANI.Size = new System.Drawing.Size(126, 20);
            this.txtPINNumberANI.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(53, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "ANI:";
            // 
            // tpPOPSerno1
            // 
            this.tpPOPSerno1.Controls.Add(this.groupBox7);
            this.tpPOPSerno1.Location = new System.Drawing.Point(4, 22);
            this.tpPOPSerno1.Name = "tpPOPSerno1";
            this.tpPOPSerno1.Size = new System.Drawing.Size(565, 149);
            this.tpPOPSerno1.TabIndex = 6;
            this.tpPOPSerno1.Text = "CTI_POP_Serno (#1)";
            this.tpPOPSerno1.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label33);
            this.groupBox7.Controls.Add(this.label14);
            this.groupBox7.Controls.Add(this.txtPopSerno1NMSiteId);
            this.groupBox7.Controls.Add(this.btnPopSerno1SendDDE);
            this.groupBox7.Controls.Add(this.txtPopSerno1CallId);
            this.groupBox7.Controls.Add(this.label15);
            this.groupBox7.Controls.Add(this.txtPopSerno1ANI);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox7.Location = new System.Drawing.Point(0, 0);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(565, 149);
            this.groupBox7.TabIndex = 4;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Parameters";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(219, 101);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(228, 13);
            this.label33.TabIndex = 7;
            this.label33.Text = "CTI_POP_Serno,<ANI>,<Call Id>,<NM Site Id>";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(19, 73);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "NM Site Id:";
            // 
            // txtPopSerno1NMSiteId
            // 
            this.txtPopSerno1NMSiteId.Location = new System.Drawing.Point(87, 70);
            this.txtPopSerno1NMSiteId.Name = "txtPopSerno1NMSiteId";
            this.txtPopSerno1NMSiteId.Size = new System.Drawing.Size(126, 20);
            this.txtPopSerno1NMSiteId.TabIndex = 5;
            // 
            // btnPopSerno1SendDDE
            // 
            this.btnPopSerno1SendDDE.Location = new System.Drawing.Point(87, 96);
            this.btnPopSerno1SendDDE.Name = "btnPopSerno1SendDDE";
            this.btnPopSerno1SendDDE.Size = new System.Drawing.Size(126, 23);
            this.btnPopSerno1SendDDE.TabIndex = 4;
            this.btnPopSerno1SendDDE.Text = "Send DDE Command";
            this.btnPopSerno1SendDDE.UseVisualStyleBackColor = true;
            this.btnPopSerno1SendDDE.Click += new System.EventHandler(this.btnPopSerno1SendDDE_Click);
            // 
            // txtPopSerno1CallId
            // 
            this.txtPopSerno1CallId.Location = new System.Drawing.Point(87, 44);
            this.txtPopSerno1CallId.Name = "txtPopSerno1CallId";
            this.txtPopSerno1CallId.Size = new System.Drawing.Size(126, 20);
            this.txtPopSerno1CallId.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(40, 47);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Call Id:";
            // 
            // txtPopSerno1ANI
            // 
            this.txtPopSerno1ANI.Location = new System.Drawing.Point(87, 17);
            this.txtPopSerno1ANI.Name = "txtPopSerno1ANI";
            this.txtPopSerno1ANI.Size = new System.Drawing.Size(126, 20);
            this.txtPopSerno1ANI.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(53, 20);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(28, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "ANI:";
            // 
            // tpPOPSerno2
            // 
            this.tpPOPSerno2.Controls.Add(this.groupBox8);
            this.tpPOPSerno2.Location = new System.Drawing.Point(4, 22);
            this.tpPOPSerno2.Name = "tpPOPSerno2";
            this.tpPOPSerno2.Size = new System.Drawing.Size(565, 149);
            this.tpPOPSerno2.TabIndex = 7;
            this.tpPOPSerno2.Text = "CTI_POP_Serno (#2)";
            this.tpPOPSerno2.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label34);
            this.groupBox8.Controls.Add(this.label20);
            this.groupBox8.Controls.Add(this.txtPopSerno2SerialNumber);
            this.groupBox8.Controls.Add(this.btnPopSerno2SendDDE);
            this.groupBox8.Controls.Add(this.txtPopSerno2CallId);
            this.groupBox8.Controls.Add(this.label21);
            this.groupBox8.Controls.Add(this.txtPopSerno2ANI);
            this.groupBox8.Controls.Add(this.label22);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox8.Location = new System.Drawing.Point(0, 0);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(565, 149);
            this.groupBox8.TabIndex = 5;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Parameters";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(219, 101);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(244, 13);
            this.label34.TabIndex = 8;
            this.label34.Text = "CTI_POP_Serno,<ANI>,<Call Id>,<Serial Number>";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(5, 73);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(76, 13);
            this.label20.TabIndex = 6;
            this.label20.Text = "Serial Number:";
            // 
            // txtPopSerno2SerialNumber
            // 
            this.txtPopSerno2SerialNumber.Location = new System.Drawing.Point(87, 70);
            this.txtPopSerno2SerialNumber.Name = "txtPopSerno2SerialNumber";
            this.txtPopSerno2SerialNumber.Size = new System.Drawing.Size(126, 20);
            this.txtPopSerno2SerialNumber.TabIndex = 5;
            // 
            // btnPopSerno2SendDDE
            // 
            this.btnPopSerno2SendDDE.Location = new System.Drawing.Point(87, 96);
            this.btnPopSerno2SendDDE.Name = "btnPopSerno2SendDDE";
            this.btnPopSerno2SendDDE.Size = new System.Drawing.Size(126, 23);
            this.btnPopSerno2SendDDE.TabIndex = 4;
            this.btnPopSerno2SendDDE.Text = "Send DDE Command";
            this.btnPopSerno2SendDDE.UseVisualStyleBackColor = true;
            this.btnPopSerno2SendDDE.Click += new System.EventHandler(this.btnPopSerno2SendDDE_Click);
            // 
            // txtPopSerno2CallId
            // 
            this.txtPopSerno2CallId.Location = new System.Drawing.Point(87, 44);
            this.txtPopSerno2CallId.Name = "txtPopSerno2CallId";
            this.txtPopSerno2CallId.Size = new System.Drawing.Size(126, 20);
            this.txtPopSerno2CallId.TabIndex = 3;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(40, 47);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(39, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "Call Id:";
            // 
            // txtPopSerno2ANI
            // 
            this.txtPopSerno2ANI.Location = new System.Drawing.Point(87, 17);
            this.txtPopSerno2ANI.Name = "txtPopSerno2ANI";
            this.txtPopSerno2ANI.Size = new System.Drawing.Size(126, 20);
            this.txtPopSerno2ANI.TabIndex = 1;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(53, 20);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(28, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "ANI:";
            // 
            // tpCheckCall
            // 
            this.tpCheckCall.Controls.Add(this.groupBox9);
            this.tpCheckCall.Location = new System.Drawing.Point(4, 22);
            this.tpCheckCall.Name = "tpCheckCall";
            this.tpCheckCall.Size = new System.Drawing.Size(565, 149);
            this.tpCheckCall.TabIndex = 8;
            this.tpCheckCall.Text = "CTI_CheckCall";
            this.tpCheckCall.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label35);
            this.groupBox9.Controls.Add(this.btnCheckCallSendDDE);
            this.groupBox9.Controls.Add(this.txtCheckCallCallId);
            this.groupBox9.Controls.Add(this.label24);
            this.groupBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox9.Location = new System.Drawing.Point(0, 0);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(565, 149);
            this.groupBox9.TabIndex = 6;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Parameters";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(219, 50);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(122, 13);
            this.label35.TabIndex = 5;
            this.label35.Text = "CTI_CheckCall,<Call Id>";
            // 
            // btnCheckCallSendDDE
            // 
            this.btnCheckCallSendDDE.Location = new System.Drawing.Point(87, 45);
            this.btnCheckCallSendDDE.Name = "btnCheckCallSendDDE";
            this.btnCheckCallSendDDE.Size = new System.Drawing.Size(126, 23);
            this.btnCheckCallSendDDE.TabIndex = 4;
            this.btnCheckCallSendDDE.Text = "Send DDE Command";
            this.btnCheckCallSendDDE.UseVisualStyleBackColor = true;
            this.btnCheckCallSendDDE.Click += new System.EventHandler(this.btnCheckCallSendDDE_Click);
            // 
            // txtCheckCallCallId
            // 
            this.txtCheckCallCallId.Location = new System.Drawing.Point(87, 19);
            this.txtCheckCallCallId.Name = "txtCheckCallCallId";
            this.txtCheckCallCallId.Size = new System.Drawing.Size(126, 20);
            this.txtCheckCallCallId.TabIndex = 3;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(40, 22);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(39, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Call Id:";
            // 
            // tpAcceptCase
            // 
            this.tpAcceptCase.Controls.Add(this.groupBox10);
            this.tpAcceptCase.Location = new System.Drawing.Point(4, 22);
            this.tpAcceptCase.Name = "tpAcceptCase";
            this.tpAcceptCase.Size = new System.Drawing.Size(565, 149);
            this.tpAcceptCase.TabIndex = 9;
            this.tpAcceptCase.Text = "CTI_AcceptCase";
            this.tpAcceptCase.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label36);
            this.groupBox10.Controls.Add(this.btnAcceptCaseSendDDE);
            this.groupBox10.Controls.Add(this.txtAcceptCaseCaseNumber);
            this.groupBox10.Controls.Add(this.label23);
            this.groupBox10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox10.Location = new System.Drawing.Point(0, 0);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(565, 149);
            this.groupBox10.TabIndex = 7;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Parameters";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(219, 50);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(167, 13);
            this.label36.TabIndex = 5;
            this.label36.Text = "CTI_AcceptCase,<Case Number>";
            // 
            // btnAcceptCaseSendDDE
            // 
            this.btnAcceptCaseSendDDE.Location = new System.Drawing.Point(87, 45);
            this.btnAcceptCaseSendDDE.Name = "btnAcceptCaseSendDDE";
            this.btnAcceptCaseSendDDE.Size = new System.Drawing.Size(126, 23);
            this.btnAcceptCaseSendDDE.TabIndex = 4;
            this.btnAcceptCaseSendDDE.Text = "Send DDE Command";
            this.btnAcceptCaseSendDDE.UseVisualStyleBackColor = true;
            this.btnAcceptCaseSendDDE.Click += new System.EventHandler(this.btnAcceptCaseSendDDE_Click);
            // 
            // txtAcceptCaseCaseNumber
            // 
            this.txtAcceptCaseCaseNumber.Location = new System.Drawing.Point(87, 19);
            this.txtAcceptCaseCaseNumber.Name = "txtAcceptCaseCaseNumber";
            this.txtAcceptCaseCaseNumber.Size = new System.Drawing.Size(126, 20);
            this.txtAcceptCaseCaseNumber.TabIndex = 3;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 22);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(74, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "Case Number:";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 153);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(573, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(87, 71);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Send DDE Command";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(87, 44);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(126, 20);
            this.textBox5.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Call ID:";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(87, 17);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(126, 20);
            this.textBox6.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Case Number:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(19, 73);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(62, 13);
            this.label17.TabIndex = 6;
            this.label17.Text = "NM Site ID:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(87, 70);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(126, 20);
            this.textBox1.TabIndex = 5;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(87, 96);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(126, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Send DDE Command";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(87, 44);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(126, 20);
            this.textBox2.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(40, 47);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Call ID:";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(87, 17);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(126, 20);
            this.textBox3.TabIndex = 1;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(53, 20);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(28, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "ANI:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 175);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "DDE Test: CIC Plug-in -> Clarify";
            this.tabControl1.ResumeLayout(false);
            this.tpSettings.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.tpShowCase.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tpNPopPhone.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tpPOPPinDirect1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tpPOPPinDirect2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tpPOPPMCCDirect.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tpPOPPinNumber.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tpPOPSerno1.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.tpPOPSerno2.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tpCheckCall.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.tpAcceptCase.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpShowCase;
        private System.Windows.Forms.TabPage tpNPopPhone;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtShowCaseCaseNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtNPopPhoneCallId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNPopPhoneANI;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button btnNPopPhoneSendDDE;
        private System.Windows.Forms.Button btnShowCaseSendDDE;
        private System.Windows.Forms.TabPage tpPOPPinDirect1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnPOPPinDirect1SendDDE;
        private System.Windows.Forms.TextBox txtPOPPinDirect1CustomerPin;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtShowCaseCallId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tpPOPPinDirect2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnPopPinDirect2SendDDE;
        private System.Windows.Forms.TextBox txtPOPPinDirect2CallId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPOPPinDirect2ANI;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tpPOPPMCCDirect;
        private System.Windows.Forms.TabPage tpPOPPinNumber;
        private System.Windows.Forms.TabPage tpPOPSerno1;
        private System.Windows.Forms.TabPage tpPOPSerno2;
        private System.Windows.Forms.TabPage tpCheckCall;
        private System.Windows.Forms.TabPage tpAcceptCase;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnPMCCDirectSendDDE;
        private System.Windows.Forms.TextBox txtPMCCDirectPMCCSiteID;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtPINNumberPINNumber;
        private System.Windows.Forms.Button btnPINNumberSendDDE;
        private System.Windows.Forms.TextBox txtPINNumberCallId;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtPINNumberANI;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtPopSerno1NMSiteId;
        private System.Windows.Forms.Button btnPopSerno1SendDDE;
        private System.Windows.Forms.TextBox txtPopSerno1CallId;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtPopSerno1ANI;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtPopSerno2SerialNumber;
        private System.Windows.Forms.Button btnPopSerno2SendDDE;
        private System.Windows.Forms.TextBox txtPopSerno2CallId;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtPopSerno2ANI;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button btnCheckCallSendDDE;
        private System.Windows.Forms.TextBox txtCheckCallCallId;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button btnAcceptCaseSendDDE;
        private System.Windows.Forms.TextBox txtAcceptCaseCaseNumber;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TabPage tpSettings;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox txtDDETopic;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtDDEService;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
    }
}

